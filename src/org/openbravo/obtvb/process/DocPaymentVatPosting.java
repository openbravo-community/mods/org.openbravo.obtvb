/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2009-2016 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */
package org.openbravo.obtvb.process;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;

import org.apache.log4j.Logger;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.dal.service.OBQuery;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.erpCommon.ad_forms.Account;
import org.openbravo.erpCommon.ad_forms.AcctSchema;
import org.openbravo.erpCommon.ad_forms.AcctServer;
import org.openbravo.erpCommon.ad_forms.DocLine_Payment;
import org.openbravo.erpCommon.ad_forms.DocPayment;
import org.openbravo.erpCommon.ad_forms.DocPaymentTemplate;
import org.openbravo.erpCommon.ad_forms.DocTax;
import org.openbravo.erpCommon.ad_forms.Fact;
import org.openbravo.erpCommon.utility.SequenceIdData;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.model.common.invoice.Invoice;
import org.openbravo.model.common.invoice.InvoiceTax;
import org.openbravo.model.financialmgmt.gl.GLItem;
import org.openbravo.model.financialmgmt.gl.GLItemAccounts;
import org.openbravo.model.financialmgmt.payment.DebtPayment;
import org.openbravo.model.financialmgmt.payment.DebtPaymentBalancing;
import org.openbravo.model.financialmgmt.payment.Settlement;
import org.openbravo.model.financialmgmt.tax.TaxRateAccounts;
import org.openbravo.model.financialmgmt.tax.WithholdingAccounts;

public class DocPaymentVatPosting extends DocPaymentTemplate {
  @SuppressWarnings("unused")
  private static final long serialVersionUID = 1L;
  static Logger log4j = Logger.getLogger(DocPaymentVatPosting.class);
  private String SeqNo = "0";
  static final BigDecimal ZERO = BigDecimal.ZERO;

  /**
   * Create Facts (the accounting logic) for STT, APP.
   * 
   * <pre>
   * 
   *  Flow:
   *    1. Currency conversion variations
   *    2. Non manual DPs in settlement
   *       2.1 Cancelled
   *       2.2 Generated
   *    3. Manual DPs in settlement
   *       3.1 Transitory account
   *    4. Conceptos contables (manual settlement and cancellation DP)
   *    5. Writeoff
   *    6. Bank in transit
   * 
   * </pre>
   * 
   * @param as
   *          accounting schema
   * @return Fact
   */
  public Fact createFact(DocPayment docPayment, AcctSchema as, ConnectionProvider conn,
      Connection con, VariablesSecureApp vars) throws ServletException {
    Fact fact = new Fact(docPayment, as, Fact.POST_Actual);
    try {
      OBContext.setAdminMode(true);
      Organization org = (Organization) OBDal.getInstance().get(Organization.class,
          docPayment.AD_Org_ID);
      String Fact_Acct_Group_ID = SequenceIdData.getUUID();

      if (log4j.isDebugEnabled())
        log4j.debug("DocPayment - createFact - p_lines.length - " + docPayment.p_lines.length);

      for (int i = 0; docPayment.p_lines != null && i < docPayment.p_lines.length; i++) {
        DocLine_Payment line = (DocLine_Payment) docPayment.p_lines[i];
        DebtPayment payment = (DebtPayment) OBDal.getInstance().get(DebtPayment.class,
            line.getLine_ID());

        if (log4j.isDebugEnabled())
          log4j
              .debug("DocPayment - createFact - line.conversionDate - " + line.getConversionDate());
        // 1* Amount is calculated and if there is currency conversion
        // variations between dates this change is accounted
        String convertedAmt = docPayment.convertAmount(line.getAmount(), line.getIsReceipt()
            .equals("Y"), docPayment.DateAcct, line.getConversionDate(), line
            .getC_Currency_ID_From(), docPayment.C_Currency_ID, line, as, fact, Fact_Acct_Group_ID,
            conn);
        String convertWithHold = docPayment.convertAmount(line.getWithHoldAmt(), line
            .getIsReceipt().equals("Y"), docPayment.DateAcct, line.getConversionDate(), line
            .getC_Currency_ID_From(), docPayment.C_Currency_ID, line, as, fact, Fact_Acct_Group_ID,
            conn);
        BigDecimal convertTotal = new BigDecimal(convertedAmt).add(new BigDecimal(convertWithHold));

        if (line.getIsManual().equals("N")) { // 2* Normal debt-payments
          String finalConvertedAmt = "";
          if (!docPayment.C_Currency_ID.equals(as.m_C_Currency_ID)) {
            docPayment.MultiCurrency = true;
            // Final conversion needed when currency of the document and currency of the accounting
            // schema are different
            finalConvertedAmt = docPayment.convertAmount(convertTotal.toString(), line
                .getIsReceipt().equals("Y"), docPayment.DateAcct, line.getConversionDate(),
                docPayment.C_Currency_ID, as.m_C_Currency_ID, null, as, fact, Fact_Acct_Group_ID,
                conn);
          } else
            finalConvertedAmt = convertTotal.toString();
          if (!line.getC_Settlement_Generate_ID().equals(docPayment.Record_ID)) { // 2.1*
            // Cancelled
            // DP
            finalConvertedAmt = AcctServer.getConvertedAmt(finalConvertedAmt, as.m_C_Currency_ID,
                docPayment.C_Currency_ID, docPayment.DateAcct, "", docPayment.AD_Client_ID,
                docPayment.AD_Org_ID, conn);
            fact.createLine(line, docPayment.getAccountBPartner(line.m_C_BPartner_ID, as, line
                .getIsReceipt().equals("Y"), line.getDpStatus(), conn), docPayment.C_Currency_ID,
                (line.getIsReceipt().equals("Y") ? "" : finalConvertedAmt), (line.getIsReceipt()
                    .equals("Y") ? finalConvertedAmt : ""), Fact_Acct_Group_ID, nextSeqNo(SeqNo),
                docPayment.DocumentType, conn);
            // Timing of VAT
            if (line.getIsPaid().equals("Y"))
              fact = addTaxesFromPayment(docPayment, as, conn, fact, payment, line, new BigDecimal(
                  "1"), org.isObtvbIssettlementvat());
          } else { // 2.2* Generated DP
            if (log4j.isDebugEnabled())
              log4j.debug("Genenarted DP");
            if (!line.getIsPaid().equals("Y")
                || !(line.getC_Settlement_Cancel_ID() == null || line.getC_Settlement_Cancel_ID()
                    .equals(""))) {
              if (log4j.isDebugEnabled())
                log4j.debug("Not paid");
              fact.createLine(line, docPayment.getAccountBPartner(line.m_C_BPartner_ID, as, line
                  .getIsReceipt().equals("Y"), line.getDpStatus(), conn), docPayment.C_Currency_ID,
                  (line.getIsReceipt().equals("Y") ? convertTotal.toString() : ""), (line
                      .getIsReceipt().equals("Y") ? "" : convertTotal.toString()),
                  Fact_Acct_Group_ID, nextSeqNo(SeqNo), docPayment.DocumentType, conn);
            }
          }

          if (log4j.isDebugEnabled())
            log4j.debug("DocPayment - createFact - No manual  - isReceipt: " + line.getIsReceipt());
        } else {// 3* MANUAL debt-payments (generated in a Manual stt)
          if (log4j.isDebugEnabled())
            log4j.debug("Manual DP - DirectPosting: " + line.getIsDirectPosting() + " - SettType:"
                + docPayment.getSettlementType());
          if (line.getIsDirectPosting().equals("Y")) { // Direct posting:
            // transitory Account
            BigDecimal amount = ZERO;
            if (log4j.isDebugEnabled())
              log4j.debug("data[0].amount:" + payment.getAmount().toString() + " - convertedAmt:"
                  + convertedAmt);

            if (convertedAmt != null && !convertedAmt.equals(""))
              amount = new BigDecimal(convertedAmt);
            boolean changeGenerate = (!docPayment.getSettlementType().equals("I"));
            if (changeGenerate)
              amount = amount.negate();
            BigDecimal transitoryAmount = new BigDecimal(convertedAmt);
            if (log4j.isDebugEnabled())
              log4j.debug("Manual DP - amount:" + amount + " - transitoryAmount:"
                  + transitoryAmount + " - Receipt:" + line.getIsReceipt());
            // Line is cloned to add withholding and/or tax info
            DocLine_Payment lineAux = DocLine_Payment.clone(line);
            lineAux.setM_C_WithHolding_ID(payment.getWithholding().getId());
            lineAux.setM_C_Tax_ID(payment.getGLItem().getTax().getId());
            // Depending on the stt type and the signum of DP it will be
            // posted on credit or debit
            GLItemAccounts glItemsAccount = getGLItemAcct(payment.getGLItem(), as);

            if (amount.signum() == 1) {
              fact.createLine(lineAux, new Account(conn,
                  (lineAux.getIsReceipt().equals("Y") ? glItemsAccount.getGlitemCreditAcct()
                      .getId() : glItemsAccount.getGlitemDebitAcct().getId())),
                  docPayment.C_Currency_ID, (lineAux.getIsReceipt().equals("Y") ? transitoryAmount
                      .abs().toString() : "0"), (lineAux.getIsReceipt().equals("Y") ? "0"
                      : transitoryAmount.abs().toString()), Fact_Acct_Group_ID, nextSeqNo(SeqNo),
                  docPayment.DocumentType, conn);
              if ((!changeGenerate && line.getIsReceipt().equals("N"))
                  || (changeGenerate && line.getIsReceipt().equals("Y")))
                amount = amount.negate();
            } else {
              fact.createLine(lineAux, new Account(conn,
                  (lineAux.getIsReceipt().equals("Y") ? glItemsAccount.getGlitemCreditAcct()
                      .getId() : glItemsAccount.getGlitemDebitAcct().getId())),
                  docPayment.C_Currency_ID, (lineAux.getIsReceipt().equals("Y") ? "0"
                      : transitoryAmount.abs().toString()),
                  (lineAux.getIsReceipt().equals("Y") ? transitoryAmount.abs().toString() : "0"),
                  Fact_Acct_Group_ID, nextSeqNo(SeqNo), docPayment.DocumentType, conn);
              if ((!changeGenerate && line.getIsReceipt().equals("Y"))
                  || (changeGenerate && line.getIsReceipt().equals("N")))
                amount = amount.negate();
            }
          }
          // 4 Manual Settlement + Cancellation Settlement (no direct posting)
          // G/L items
          if (docPayment.getSettlementType().equals("I") || line.getIsDirectPosting().equals("N")) {
            final StringBuilder whereClause = new StringBuilder();
            whereClause.append(" as cdpbal ");
            whereClause.append(" where cdpbal.payment.id = '" + payment.getId() + "'");
            final OBQuery<DebtPaymentBalancing> obqParameters = OBDal.getInstance().createQuery(
                DebtPaymentBalancing.class, whereClause.toString());
            final List<DebtPaymentBalancing> debtPaymentBalancing = obqParameters.list();

            // for (int j = 0; data != null && j < data.length; j++) {
            for (DebtPaymentBalancing dpbal : debtPaymentBalancing) {
              String amountdebit = AcctServer.getConvertedAmt(dpbal.getDebitAmount().toString(),
                  line.getC_Currency_ID_From(), docPayment.C_Currency_ID, docPayment.DateAcct, "",
                  docPayment.AD_Client_ID, docPayment.AD_Org_ID, conn);
              String amountcredit = AcctServer.getConvertedAmt(dpbal.getCreditAmount().toString(),
                  line.getC_Currency_ID_From(), docPayment.C_Currency_ID, docPayment.DateAcct, "",
                  docPayment.AD_Client_ID, docPayment.AD_Org_ID, conn);
              if (log4j.isDebugEnabled())
                log4j.debug("DocPayment - createFact - Conceptos - AmountDebit: " + amountdebit
                    + " - AmountCredit: " + amountcredit);
              // Line is cloned to add withholding and/or tax info
              DocLine_Payment lineAux = DocLine_Payment.clone(line);
              lineAux.setM_C_WithHolding_ID(dpbal.getGLItem().getWithholding().getId());
              lineAux.setM_C_Tax_ID(dpbal.getGLItem().getTax().getId());
              GLItemAccounts glItemAccount = getGLItemAcct(payment.getGLItem(), as);
              fact.createLine(lineAux, new Account(conn,
                  (lineAux.getIsReceipt().equals("Y") ? glItemAccount.getGlitemCreditAcct().getId()
                      : glItemAccount.getGlitemDebitAcct().getId())), docPayment.C_Currency_ID,
                  (amountdebit.equals("0") ? "" : amountdebit), (amountcredit.equals("0") ? ""
                      : amountcredit), Fact_Acct_Group_ID, nextSeqNo(SeqNo),
                  docPayment.DocumentType, conn);
            }
          }
        } // END debt-payment conditions

        // 5* WRITEOFF calculations
        if (line.getC_Settlement_Cancel_ID().equals(docPayment.Record_ID)) { // Cancelled
          // debt-payments
          if (line.getWriteOffAmt() != null && !line.getWriteOffAmt().equals("")
              && !line.getWriteOffAmt().equals("0")) {
            fact.createLine(line, docPayment.getAccount(AcctServer.ACCTTYPE_WriteOffDefault, as,
                conn), docPayment.C_Currency_ID,
                (line.getIsReceipt().equals("Y") ? line.getWriteOffAmt() : ""), (line
                    .getIsReceipt().equals("Y") ? "" : line.getWriteOffAmt()), Fact_Acct_Group_ID,
                docPayment.nextSeqNo(SeqNo), docPayment.DocumentType, conn);
          }
        }

        // 6* PPA - Bank in transit default, paid DPs, (non manual and
        // manual non direct posting)
        if (line.getIsPaid().equals("Y")
            && ((line.getC_Settlement_Cancel_ID() == null || line.getC_Settlement_Cancel_ID()
                .equals("")) || (line.getC_Settlement_Cancel_ID().equals(docPayment.Record_ID)))) {
          BigDecimal finalLineAmt = new BigDecimal(line.getAmount());
          String idSchema = as.getC_AcctSchema_ID();
          if (line.getC_WITHHOLDING_ID() != null && !line.getC_WITHHOLDING_ID().equals("")) {
            String IdAccount = getWithHoldinAccountId(line.getC_WITHHOLDING_ID(), idSchema);
            //
            String sWithHoldAmt = AcctServer.getConvertedAmt(line.getWithHoldAmt(),
                line.getC_Currency_ID_From(), docPayment.C_Currency_ID, docPayment.DateAcct, "",
                docPayment.AD_Client_ID, docPayment.AD_Org_ID, conn);

            fact.createLine(line, Account.getAccount(conn, IdAccount), docPayment.C_Currency_ID,
                (line.getIsReceipt().equals("Y") ? sWithHoldAmt : ""),
                (line.getIsReceipt().equals("Y") ? "" : sWithHoldAmt), Fact_Acct_Group_ID,
                "999999", docPayment.DocumentType, conn);
          }
          if (line.getWriteOffAmt() != null && !line.getWriteOffAmt().equals("")
              && !line.getWriteOffAmt().equals("0"))
            finalLineAmt = finalLineAmt.subtract(new BigDecimal(line.getWriteOffAmt()));
          String finalAmtTo = AcctServer.getConvertedAmt(finalLineAmt.toString(),
              line.getC_Currency_ID_From(), docPayment.C_Currency_ID, docPayment.DateAcct, "",
              docPayment.AD_Client_ID, docPayment.AD_Org_ID, conn);
          finalLineAmt = new BigDecimal(finalAmtTo);
          if (finalLineAmt.compareTo(new BigDecimal("0.00")) != 0) {
            if (line.getC_BANKSTATEMENTLINE_ID() != null
                && !line.getC_BANKSTATEMENTLINE_ID().equals("")) {
              fact.createLine(line, docPayment.getAccountBankStatementLine(
                  line.getC_BANKSTATEMENTLINE_ID(), as, conn), docPayment.C_Currency_ID, (line
                  .getIsReceipt().equals("Y") ? finalAmtTo : ""),
                  (line.getIsReceipt().equals("Y") ? "" : finalAmtTo), Fact_Acct_Group_ID,
                  "999999", docPayment.DocumentType, conn);
            }// else if(line.C_CASHLINE_ID!=null &&
             // !line.C_CASHLINE_ID.equals("")) fact.createLine(line,
             // getAccountCashLine(line.C_CASHLINE_ID,
             // as,conn),C_Currency_ID,
             // (line.isReceipt.equals("Y")?finalAmtTo:""),(line.isReceipt.equals("Y")?"":finalAmtTo),
             // Fact_Acct_Group_ID, "999999", DocumentType,conn);
            else
              fact.createLine(line,
                  docPayment.getAccount(AcctServer.ACCTTYPE_BankInTransitDefault, as, conn),
                  docPayment.C_Currency_ID, (line.getIsReceipt().equals("Y") ? finalAmtTo : ""),
                  (line.getIsReceipt().equals("Y") ? "" : finalAmtTo), Fact_Acct_Group_ID,
                  "999999", docPayment.DocumentType, conn);
          }
        }
      } // END of the C_Debt_Payment loop
      SeqNo = "0";
      if (log4j.isDebugEnabled())
        log4j.debug("DocPayment - createFact - finish");
    } finally {
      OBContext.restorePreviousMode();
    }
    return fact;
  }

  /**
   * 
   * @param oldSeqNo
   * @return
   */
  public String nextSeqNo(String oldSeqNo) {
    log4j.debug("DocPayment - oldSeqNo = " + oldSeqNo);
    BigDecimal seqNo = new BigDecimal(oldSeqNo);
    SeqNo = (seqNo.add(new BigDecimal("10"))).toString();
    log4j.debug("DocPayment - nextSeqNo = " + SeqNo);
    return SeqNo;
  }

  /**
   * 
   * @param strWithholdingId
   * @param strAcctSchemaId
   * @param string
   * @return
   */
  public String getWithHoldinAccountId(String strWithholdingId, String strAcctSchemaId) {
    try {
      OBContext.setAdminMode(true);
      final StringBuilder whereClause = new StringBuilder();
      whereClause.append(" as whacct ");
      whereClause.append(" where whacct.withholding.id = '" + strWithholdingId + "'");
      whereClause.append(" and glia.accountingSchema.id = '" + strAcctSchemaId + "'");
      final OBQuery<WithholdingAccounts> obqParameters = OBDal.getInstance().createQuery(
          WithholdingAccounts.class, whereClause.toString());
      final List<WithholdingAccounts> withholdingAccounts = obqParameters.list();
      return withholdingAccounts.get(0).getWithholdingAccount().getId();
    } finally {
      OBContext.restorePreviousMode();
    }
  }

  /**
   * 
   * @param payment
   * @param as
   * @return
   */
  public GLItemAccounts getGLItemAcct(GLItem glitem, AcctSchema as) {
    try {
      OBContext.setAdminMode(true);
      final StringBuilder whereClause = new StringBuilder();
      whereClause.append(" as glia ");
      whereClause.append(" where glia.gLItem.id = '" + glitem.getId() + "'");
      whereClause.append(" and glia.accountingSchema.id = '" + as.m_C_AcctSchema_ID + "'");
      final OBQuery<GLItemAccounts> obqParameters = OBDal.getInstance().createQuery(
          GLItemAccounts.class, whereClause.toString());
      final List<GLItemAccounts> glItemsAccounts = obqParameters.list();
      return glItemsAccounts.get(0);
    } finally {
      OBContext.restorePreviousMode();
    }
  }

  /**
   * 
   * @param docPayment
   * @param as
   * @param conn
   * @param fact
   * @param payment
   * @param docLine
   * @param multipier
   * @param isPostVATInSettlement
   * @return
   */
  public Fact addTaxesFromPayment(DocPayment docPayment, AcctSchema as, ConnectionProvider conn,
      Fact fact, DebtPayment payment, DocLine_Payment docLine, BigDecimal multipier,
      boolean isPostVATInSettlement) {
    if (!isPostVATInSettlement)
      return fact;
    Fact factResult = fact;
    if (payment.getInvoice() != null)
      factResult = createlineTaxesFromInvoice(payment.getInvoice(), fact, payment, docLine, as,
          docPayment.C_Currency_ID, docPayment.DocumentType, conn, multipier);
    else {
      Settlement settlement = payment.getSettlementGenerate();
      List<DebtPayment> sourcePayments = settlement
          .getFinancialMgmtDebtPaymentSettlementCancelledList();
      BigDecimal settlementNotPaidAmount = new BigDecimal("0.00");
      for (DebtPayment sourcePayment : sourcePayments)
        if (!sourcePayment.isPaymentComplete())
          settlementNotPaidAmount = settlementNotPaidAmount.add(sourcePayment.getAmount());
      for (DebtPayment sourcePayment : sourcePayments)
        if (!sourcePayment.isPaymentComplete() && sourcePayment.getInvoice() != null)
          factResult = createlineTaxesFromInvoice(
              sourcePayment.getInvoice(),
              factResult,
              sourcePayment,
              docLine,
              as,
              docPayment.C_Currency_ID,
              docPayment.DocumentType,
              conn,
              payment.getAmount().multiply(multipier)
                  .divide(settlementNotPaidAmount, 1000, RoundingMode.HALF_UP));
        else if (!sourcePayment.isPaymentComplete())
          addTaxesFromPayment(
              docPayment,
              as,
              conn,
              factResult,
              sourcePayment,
              docLine,
              payment.getAmount().multiply(multipier)
                  .divide(settlementNotPaidAmount, 1000, RoundingMode.HALF_UP), true);
    }
    return factResult;
  }

  /**
   * 
   * @param invoice
   * @param fact
   * @param payment
   * @param docLine
   * @param as
   * @param C_Currency_ID
   * @param DocumentType
   * @param conn
   * @param multiplier
   * @return
   */
  public Fact createlineTaxesFromInvoice(Invoice invoice, Fact fact, DebtPayment payment,
      DocLine_Payment docLine, AcctSchema as, String C_Currency_ID, String DocumentType,
      ConnectionProvider conn, BigDecimal multiplier) {
    if (!invoice.isSalesTransaction())// Just applied to sales transactions
      return fact;
    DocTax[] taxes = loadTaxes(invoice);
    for (int i = 0; i < taxes.length; i++) {
      BigDecimal amount = new BigDecimal(taxes[i].m_amount).multiply(multiplier)
          .multiply(payment.getAmount())
          .divide(invoice.getGrandTotalAmount(), 1000, RoundingMode.HALF_UP);
      amount = amount.setScale(invoice.getCurrency().getStandardPrecision().intValue(),
          RoundingMode.HALF_UP);
      String strFact_Acct_Group_ID = SequenceIdData.getUUID();
      if (invoice.getDocumentType().getDocumentCategory().equals("ARI")
          || invoice.getDocumentType().getDocumentCategory().equals("ARP")
          || invoice.getDocumentType().getDocumentCategory().equals("ARI_RM")) {
        fact.createLine(docLine, taxes[i].getAccount(DocTax.ACCTTYPE_TaxDue, as, conn),
            C_Currency_ID, "", amount.toString(), strFact_Acct_Group_ID, nextSeqNo(SeqNo),
            DocumentType, conn);
        fact.createLine(docLine, getTransitoryTaxAccount(taxes[i].m_C_Tax_ID, as, conn),
            C_Currency_ID, amount.toString(), "", strFact_Acct_Group_ID, nextSeqNo(SeqNo),
            DocumentType, conn);
      } else if (invoice.getDocumentType().getDocumentCategory().equals("ARC")) {
        fact.createLine(docLine, taxes[i].getAccount(DocTax.ACCTTYPE_TaxDue, as, conn),
            C_Currency_ID, amount.toString(), "", strFact_Acct_Group_ID, nextSeqNo(SeqNo),
            DocumentType, conn);
        fact.createLine(docLine, getTransitoryTaxAccount(taxes[i].m_C_Tax_ID, as, conn),
            C_Currency_ID, "", amount.toString(), strFact_Acct_Group_ID, nextSeqNo(SeqNo),
            DocumentType, conn);
      } else if (invoice.getDocumentType().getDocumentCategory().equals("API")) {
        fact.createLine(docLine, taxes[i].getAccount(DocTax.ACCTTYPE_TaxCredit, as, conn),
            C_Currency_ID, amount.toString(), "", strFact_Acct_Group_ID, nextSeqNo(SeqNo),
            DocumentType, conn);
        fact.createLine(docLine, getTransitoryTaxAccount(taxes[i].m_C_Tax_ID, as, conn),
            C_Currency_ID, "", amount.toString(), strFact_Acct_Group_ID, nextSeqNo(SeqNo),
            DocumentType, conn);
      } else if (invoice.getDocumentType().getDocumentCategory().equals("APC")) {
        fact.createLine(docLine, taxes[i].getAccount(DocTax.ACCTTYPE_TaxCredit, as, conn),
            C_Currency_ID, "", amount.toString(), strFact_Acct_Group_ID, nextSeqNo(SeqNo),
            DocumentType, conn);
        fact.createLine(docLine, getTransitoryTaxAccount(taxes[i].m_C_Tax_ID, as, conn),
            C_Currency_ID, amount.toString(), "", strFact_Acct_Group_ID, nextSeqNo(SeqNo),
            DocumentType, conn);
      }
    }
    return fact;
  }

  /**
   * 
   * @param invoice
   * @return
   */
  public DocTax[] loadTaxes(Invoice invoice) {
    try {
      OBContext.setAdminMode(true);
      final StringBuilder whereClause = new StringBuilder();
      whereClause.append(" as invtax ");
      whereClause.append(" where invtax.invoice.id = '" + invoice.getId() + "'");
      final OBQuery<InvoiceTax> obqParameters = OBDal.getInstance().createQuery(InvoiceTax.class,
          whereClause.toString());
      // For Background process execution
      obqParameters.setFilterOnReadableClients(false);
      obqParameters.setFilterOnReadableOrganization(false);
      final List<InvoiceTax> invoiceTaxes = obqParameters.list();
      List<DocTax> docTaxes = new ArrayList<DocTax>();
      for (InvoiceTax invoiceTax : invoiceTaxes) {
        docTaxes.add(new DocTax(invoiceTax.getTax().getId(), invoiceTax.getTax().getName(),
            invoiceTax.getTax().getRate().toString(), invoiceTax.getTaxableAmount().toString(),
            invoiceTax.getTaxAmount().toString()));
      }
      DocTax[] result = new DocTax[docTaxes.size()];
      docTaxes.toArray(result);
      return result;
    } finally {
      OBContext.restorePreviousMode();
    }
  }

  /**
   * getTransitoryTaxAccount
   * 
   * @param strcTaxId
   * @param as
   * @param conn
   * @return
   */
  public Account getTransitoryTaxAccount(String strcTaxId, AcctSchema as, ConnectionProvider conn) {
    Account transitoryAccount = null;
    try {
      OBContext.setAdminMode(true);
      final StringBuilder whereClause = new StringBuilder();

      if (!strcTaxId.equals("")) {
        whereClause.append(" as tra ");
        whereClause.append(" where tra.tax.id = '" + strcTaxId + "'");
        whereClause.append(" and tra.accountingSchema.id = '" + as.m_C_AcctSchema_ID + "'");
      } else
        return null;

      final OBQuery<TaxRateAccounts> obqParameters = OBDal.getInstance().createQuery(
          TaxRateAccounts.class, whereClause.toString());
      // For Background process execution
      obqParameters.setFilterOnReadableClients(false);
      obqParameters.setFilterOnReadableOrganization(false);
      final List<TaxRateAccounts> taxRateAccounts = obqParameters.list();
      transitoryAccount = new Account(conn, taxRateAccounts.get(0).getObtvbTTransitoryAcct()
          .getId());
    } catch (ServletException e) {
      log4j.warn(e);
    } finally {
      OBContext.restorePreviousMode();
    }
    return transitoryAccount;
  } // getTransitoryTaxAccount
}
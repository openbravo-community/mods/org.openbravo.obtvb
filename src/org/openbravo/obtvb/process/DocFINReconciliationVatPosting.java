/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2011-2020 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */

package org.openbravo.obtvb.process;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Connection;

import javax.servlet.ServletException;

import org.apache.log4j.Logger;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.erpCommon.ad_forms.AcctSchema;
import org.openbravo.erpCommon.ad_forms.AcctServer;
import org.openbravo.erpCommon.ad_forms.DocFINReconciliation;
import org.openbravo.erpCommon.ad_forms.DocFINReconciliationTemplate;
import org.openbravo.erpCommon.ad_forms.DocLine_FINReconciliation;
import org.openbravo.erpCommon.ad_forms.DocTax;
import org.openbravo.erpCommon.ad_forms.Fact;
import org.openbravo.erpCommon.utility.FieldProviderFactory;
import org.openbravo.erpCommon.utility.SequenceIdData;
import org.openbravo.model.common.invoice.Invoice;
import org.openbravo.model.common.invoice.InvoiceTax;
import org.openbravo.model.common.order.Order;
import org.openbravo.model.common.order.OrderTax;
import org.openbravo.model.financialmgmt.payment.FIN_FinaccTransaction;
import org.openbravo.model.financialmgmt.payment.FIN_Payment;
import org.openbravo.model.financialmgmt.payment.FIN_PaymentDetail;
import org.openbravo.model.financialmgmt.payment.FIN_PaymentDetailV;
import org.openbravo.model.financialmgmt.tax.TaxRate;
import org.openbravo.obtvb.utillity.OBTVB_Utility;

public class DocFINReconciliationVatPosting extends DocFINReconciliationTemplate {

  static Logger log4j = Logger.getLogger(DocFINPaymentVatPosting.class);
  private static final int PERCENTAGE_SCALE = 10;

  String SeqNo = "0";

  @SuppressWarnings("static-access")
  @Override
  public Fact createFact(DocFINReconciliation docREC, AcctSchema as, ConnectionProvider conn,
      Connection con, VariablesSecureApp vars) throws ServletException {
    try {
      OBContext.setAdminMode();
      Fact fact = new Fact(docREC, as, Fact.POST_Actual);
      String Fact_Acct_Group_ID = SequenceIdData.getUUID();
      for (int i = 0; docREC.p_lines != null && i < docREC.p_lines.length; i++) {
        DocLine_FINReconciliation line = (DocLine_FINReconciliation) docREC.p_lines[i];
        FIN_FinaccTransaction transaction = OBDal.getInstance()
            .get(FIN_FinaccTransaction.class, line.getFinFinAccTransactionId());

        // 3 Scenarios: 1st Bank fee 2nd payment related transaction 3rd GL item transaction
        if (docREC.TRXTYPE_BankFee.equals(transaction.getTransactionType())) {
          fact = docREC.createFactFee(line, as, conn, fact, Fact_Acct_Group_ID);
        } else if (!"".equals(line.getFinPaymentId())) {
          fact = createFactPayment(docREC, line, as, conn, fact, Fact_Acct_Group_ID);
        } else {
          fact = docREC.createFactGLItem(line, as, conn, fact, Fact_Acct_Group_ID);
        }
      }
      return fact;
    } finally {
      OBContext.restorePreviousMode();
    }
  }

  public Fact createFactPayment(DocFINReconciliation docREC, DocLine_FINReconciliation line,
      AcctSchema as, ConnectionProvider conn, Fact fact_param, String Fact_Acct_Group_ID)
      throws ServletException {
    Fact fact = fact_param;
    FIN_Payment payment = OBDal.getInstance().get(FIN_Payment.class, line.getFinPaymentId());
    FIN_FinaccTransaction transaction = OBDal.getInstance()
        .get(FIN_FinaccTransaction.class, line.getFinFinAccTransactionId());
    if (docREC.getDocumentTransactionConfirmation(transaction)) {
      fact.createLine(line, docREC.getAccountTransactionPayment(conn, payment, as),
          docREC.C_Currency_ID, !payment.isReceipt() ? line.getAmount() : "",
          payment.isReceipt() ? line.getAmount() : "", Fact_Acct_Group_ID, docREC.nextSeqNo(SeqNo),
          docREC.DocumentType, conn);
    } else if (!docREC.getDocumentPaymentConfirmation(payment)) {
      FieldProviderFactory[] data = docREC.loadLinesPaymentDetailsFieldProvider(transaction);
      for (int i = 0; i < data.length; i++) {
        if (data[i] == null) {
          continue;
        }
        FIN_PaymentDetail paymentDetail = OBDal.getInstance()
            .get(FIN_PaymentDetail.class, data[i].getField("FIN_Payment_Detail_ID"));
        fact = createFactPaymentDetails(docREC, line, paymentDetail, as, conn, fact,
            Fact_Acct_Group_ID);
      }
      // Timing of VAT
      if (payment.getOrganization().isObtvbIssettlementvat()) {
        for (final FIN_PaymentDetailV detail : payment.getFINPaymentDetailVList()) {
          Invoice invoice;
          Order order;
          final BigDecimal paidAmt = detail.getPaidAmount();
          try {
            invoice = detail.getPaymentPlanInvoice().getInvoice();
            log4j.debug("Invoice detected: " + invoice.getIdentifier());
            if (!invoice.isSalesTransaction()) {
              invoice = null;
              log4j.debug("purchase invoice, so no timing of vat will be done");
            }
          } catch (final NullPointerException npe) {
            log4j.debug("No invoice detected");
            invoice = null;
          }
          try {
            order = detail.getOrderPaymentPlan().getSalesOrder();
            log4j.debug("Order detected: " + order.getIdentifier());
            if (!order.isSalesTransaction()) {
              order = null;
              log4j.debug("purchase order, so no timing of vat will be done");
            }
          } catch (final NullPointerException npe) {
            log4j.debug("No order detected");
            order = null;
          }

          if (invoice != null) {
            final BigDecimal docTotaAmt = invoice.getGrandTotalAmount();
            final BigDecimal percentage;
            if (docTotaAmt.compareTo(BigDecimal.ZERO) == 0) {
              percentage = BigDecimal.ZERO;
            } else {
              percentage = paidAmt.multiply(new BigDecimal("100"))
                  .divide(docTotaAmt, PERCENTAGE_SCALE, RoundingMode.HALF_UP);
            }
            log4j.debug("Document total amount: " + docTotaAmt);
            log4j.debug("Paid amount: " + paidAmt);
            log4j.debug("Percentage to apply to each tax amount: " + percentage + "%");

            for (final InvoiceTax documentTax : invoice.getInvoiceTaxList()) {
              final TaxRate tax = documentTax.getTax();
              final DocTax docTax = new DocTax(tax.getId(), tax.getName(), tax.getRate().toString(),
                  documentTax.getTaxableAmount().toString(), documentTax.getTaxAmount().toString());
              final BigDecimal taxAcctAmt = documentTax.getTaxAmount()
                  .multiply(percentage)
                  .divide(new BigDecimal("100"), 10, RoundingMode.HALF_UP);

              final String entryAmount = OBTVB_Utility.round(taxAcctAmt, docREC.C_Currency_ID)
                  .toString();
              log4j.debug("Entry amount for tax (" + tax.getIdentifier() + "): " + entryAmount
                  + " (rounded from " + taxAcctAmt + ")");

              // Enable this for two different entries
              // Fact_Acct_Group_ID = SequenceIdData.getUUID();
              if (invoice.getDocumentType().getDocumentCategory().equals("ARI")
                  || invoice.getDocumentType().getDocumentCategory().equals("ARP")
                  || invoice.getDocumentType().getDocumentCategory().equals("ARI_RM")) {
                fact.createLine(null, docTax.getAccount(DocTax.ACCTTYPE_TaxDue, as, conn),
                    docREC.C_Currency_ID, "", entryAmount, Fact_Acct_Group_ID,
                    docREC.nextSeqNo(getSeqNo()), docREC.DocumentType, conn);
                fact.createLine(null, OBTVB_Utility.getTransitoryTaxAccount(tax.getId(), as, conn),
                    docREC.C_Currency_ID, entryAmount, "", Fact_Acct_Group_ID,
                    docREC.nextSeqNo(getSeqNo()), docREC.DocumentType, conn);
              } else if (invoice.getDocumentType().getDocumentCategory().equals("ARC")) {
                fact.createLine(null, docTax.getAccount(DocTax.ACCTTYPE_TaxDue, as, conn),
                    docREC.C_Currency_ID, entryAmount, "", Fact_Acct_Group_ID,
                    docREC.nextSeqNo(getSeqNo()), docREC.DocumentType, conn);
                fact.createLine(null, OBTVB_Utility.getTransitoryTaxAccount(tax.getId(), as, conn),
                    docREC.C_Currency_ID, "", entryAmount, Fact_Acct_Group_ID,
                    docREC.nextSeqNo(getSeqNo()), docREC.DocumentType, conn);
              } else if (invoice.getDocumentType().getDocumentCategory().equals("API")) {
                fact.createLine(null, docTax.getAccount(DocTax.ACCTTYPE_TaxCredit, as, conn),
                    docREC.C_Currency_ID, entryAmount, "", Fact_Acct_Group_ID,
                    docREC.nextSeqNo(getSeqNo()), docREC.DocumentType, conn);
                fact.createLine(null, OBTVB_Utility.getTransitoryTaxAccount(tax.getId(), as, conn),
                    docREC.C_Currency_ID, "", entryAmount, Fact_Acct_Group_ID,
                    docREC.nextSeqNo(getSeqNo()), docREC.DocumentType, conn);
              } else if (invoice.getDocumentType().getDocumentCategory().equals("APC")) {
                fact.createLine(null, docTax.getAccount(DocTax.ACCTTYPE_TaxCredit, as, conn),
                    docREC.C_Currency_ID, "", entryAmount, Fact_Acct_Group_ID,
                    docREC.nextSeqNo(getSeqNo()), docREC.DocumentType, conn);
                fact.createLine(null, OBTVB_Utility.getTransitoryTaxAccount(tax.getId(), as, conn),
                    docREC.C_Currency_ID, entryAmount, "", Fact_Acct_Group_ID,
                    docREC.nextSeqNo(getSeqNo()), docREC.DocumentType, conn);
              }
            }

          } else if (order != null) {
            final BigDecimal docTotaAmt = order.getGrandTotalAmount();
            final BigDecimal percentage;
            if (docTotaAmt.compareTo(BigDecimal.ZERO) == 0) {
              percentage = BigDecimal.ZERO;
            } else {
              percentage = paidAmt.multiply(new BigDecimal("100"))
                  .divide(docTotaAmt, PERCENTAGE_SCALE, RoundingMode.HALF_UP);
            }
            log4j.debug("Document total amount: " + docTotaAmt);
            log4j.debug("Paid amount: " + paidAmt);
            log4j.debug("Percentage to apply to each tax amount: " + percentage + "%");

            for (final OrderTax documentTax : order.getOrderTaxList()) {
              final TaxRate tax = documentTax.getTax();
              DocTax docTax = new DocTax(tax.getId(), tax.getName(), tax.getRate().toString(),
                  documentTax.getTaxableAmount().toString(), documentTax.getTaxAmount().toString());
              final BigDecimal taxAcctAmt = documentTax.getTaxAmount()
                  .multiply(percentage)
                  .divide(new BigDecimal("100"), 10, RoundingMode.HALF_UP);

              final String entryAmount = OBTVB_Utility.round(taxAcctAmt, docREC.C_Currency_ID)
                  .toString();
              log4j.debug("Entry amount for tax (" + tax.getIdentifier() + "): " + entryAmount
                  + " (rounded from " + taxAcctAmt + ")");

              // Enable this for two different entries
              // Fact_Acct_Group_ID = SequenceIdData.getUUID();
              if (order.isSalesTransaction()) {
                fact.createLine(null, docTax.getAccount(DocTax.ACCTTYPE_TaxDue, as, conn),
                    docREC.C_Currency_ID, "", entryAmount, Fact_Acct_Group_ID,
                    docREC.nextSeqNo(getSeqNo()), docREC.DocumentType, conn);
                fact.createLine(null, OBTVB_Utility.getTransitoryTaxAccount(tax.getId(), as, conn),
                    docREC.C_Currency_ID, entryAmount, "", Fact_Acct_Group_ID,
                    docREC.nextSeqNo(getSeqNo()), docREC.DocumentType, conn);
              } else {
                fact.createLine(null, docTax.getAccount(DocTax.ACCTTYPE_TaxCredit, as, conn),
                    docREC.C_Currency_ID, entryAmount, "", Fact_Acct_Group_ID,
                    docREC.nextSeqNo(getSeqNo()), docREC.DocumentType, conn);
                fact.createLine(null, OBTVB_Utility.getTransitoryTaxAccount(tax.getId(), as, conn),
                    docREC.C_Currency_ID, "", entryAmount, Fact_Acct_Group_ID,
                    docREC.nextSeqNo(getSeqNo()), docREC.DocumentType, conn);
              }
            }
          } else {
            log4j.error("Neither an order nor an invoice were detected");
          }
        }

      } else {
        fact.createLine(line, docREC.getAccountPayment(conn, payment, as), docREC.C_Currency_ID,
            !payment.isReceipt() ? line.getAmount() : "",
            payment.isReceipt() ? line.getAmount() : "", Fact_Acct_Group_ID,
            docREC.nextSeqNo(SeqNo), docREC.DocumentType, conn);
      }
      fact.createLine(line, docREC.getAccountReconciliation(conn, payment, as),
          docREC.C_Currency_ID, payment.isReceipt() ? line.getAmount() : "",
          !payment.isReceipt() ? line.getAmount() : "", Fact_Acct_Group_ID, "999999",
          docREC.DocumentType, conn);
      if (!docREC.getDocumentPaymentConfirmation(payment)
          && !docREC.getDocumentTransactionConfirmation(transaction)) {
        // Pre-payment is consumed when Used Credit Amount not equals Zero. When consuming Credit no
        // credit is generated
        if (payment.getUsedCredit().compareTo(BigDecimal.ZERO) != 0
            && payment.getGeneratedCredit().compareTo(BigDecimal.ZERO) == 0) {
          fact.createLine(null,
              docREC.getAccountBPartner(payment.getBusinessPartner().getId(), as,
                  payment.isReceipt(), true, conn),
              docREC.C_Currency_ID, (payment.isReceipt() ? payment.getUsedCredit().toString() : ""),
              (payment.isReceipt() ? "" : payment.getUsedCredit().toString()), Fact_Acct_Group_ID,
              docREC.nextSeqNo(SeqNo), docREC.DocumentType, conn);
        }
      }
    }

    SeqNo = "0";
    return fact;
  }

  public Fact createFactPaymentDetails(DocFINReconciliation docREC, DocLine_FINReconciliation line,
      FIN_PaymentDetail paymentDetail, AcctSchema as, ConnectionProvider conn, Fact fact,
      String Fact_Acct_Group_ID) throws ServletException {
    boolean isPrepayment = paymentDetail.isPrepayment();
    boolean isReceipt = paymentDetail.getFinPayment().isReceipt();
    BigDecimal bpAmount = paymentDetail.getAmount();
    if (paymentDetail.getWriteoffAmount() != null
        && paymentDetail.getWriteoffAmount().compareTo(BigDecimal.ZERO) != 0) {
      fact.createLine(line, docREC.getAccount(AcctServer.ACCTTYPE_WriteOffDefault, as, conn),
          docREC.C_Currency_ID, (isReceipt ? paymentDetail.getWriteoffAmount().toString() : ""),
          (isReceipt ? "" : paymentDetail.getWriteoffAmount().toString()), Fact_Acct_Group_ID,
          docREC.nextSeqNo(SeqNo), docREC.DocumentType, conn);
      bpAmount = bpAmount.add(paymentDetail.getWriteoffAmount());
    }
    fact.createLine(line,
        docREC.getAccountBPartner(
            (line.m_C_BPartner_ID == null || line.m_C_BPartner_ID.equals("")) ? docREC.C_BPartner_ID
                : line.m_C_BPartner_ID,
            as, isReceipt, isPrepayment, conn),
        docREC.C_Currency_ID, !isReceipt ? bpAmount.toString() : "",
        isReceipt ? bpAmount.toString() : "", Fact_Acct_Group_ID, docREC.nextSeqNo(SeqNo),
        docREC.DocumentType, conn);

    SeqNo = "0";
    return fact;
  }

  public String getSeqNo() {
    return SeqNo;
  }

}

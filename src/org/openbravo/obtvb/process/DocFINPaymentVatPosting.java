/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2011-2020 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */

package org.openbravo.obtvb.process;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Connection;

import javax.servlet.ServletException;

import org.apache.log4j.Logger;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.erpCommon.ad_forms.AcctSchema;
import org.openbravo.erpCommon.ad_forms.AcctServer;
import org.openbravo.erpCommon.ad_forms.DocFINPayment;
import org.openbravo.erpCommon.ad_forms.DocFINPaymentTemplate;
import org.openbravo.erpCommon.ad_forms.DocLine_FINPayment;
import org.openbravo.erpCommon.ad_forms.DocTax;
import org.openbravo.erpCommon.ad_forms.Fact;
import org.openbravo.erpCommon.utility.SequenceIdData;
import org.openbravo.model.common.invoice.Invoice;
import org.openbravo.model.common.invoice.InvoiceTax;
import org.openbravo.model.common.order.Order;
import org.openbravo.model.common.order.OrderTax;
import org.openbravo.model.financialmgmt.gl.GLItem;
import org.openbravo.model.financialmgmt.payment.FIN_Payment;
import org.openbravo.model.financialmgmt.payment.FIN_PaymentDetailV;
import org.openbravo.model.financialmgmt.tax.TaxRate;
import org.openbravo.obtvb.utillity.OBTVB_Utility;

public class DocFINPaymentVatPosting extends DocFINPaymentTemplate {

  static Logger log4j = Logger.getLogger(DocFINPaymentVatPosting.class);
  private static final int PERCENTAGE_SCALE = 10;

  @Override
  public Fact createFact(DocFINPayment docPayment, AcctSchema as, ConnectionProvider conn,
      Connection con, VariablesSecureApp vars) throws ServletException {
    Fact fact = new Fact(docPayment, as, Fact.POST_Actual);
    String Fact_Acct_Group_ID = SequenceIdData.getUUID();
    try {
      OBContext.setAdminMode();
      for (int i = 0; docPayment.p_lines != null && i < docPayment.p_lines.length; i++) {
        DocLine_FINPayment line = (DocLine_FINPayment) docPayment.p_lines[i];

        boolean isReceipt = docPayment.DocumentType.equals("ARR");
        boolean isPrepayment = line.getIsPrepayment().equals("Y");

        String bpAmount = line.getAmount();
        if (line.getWriteOffAmt() != null && !line.getWriteOffAmt().equals("")
            && new BigDecimal(line.getWriteOffAmt()).compareTo(BigDecimal.ZERO) != 0) {
          fact.createLine(line,
              docPayment.getAccount(AcctServer.ACCTTYPE_WriteOffDefault, as, conn),
              docPayment.C_Currency_ID, (isReceipt ? line.getWriteOffAmt() : ""),
              (isReceipt ? "" : line.getWriteOffAmt()), Fact_Acct_Group_ID,
              docPayment.nextSeqNo(docPayment.getSeqNo()), docPayment.DocumentType, conn);
          bpAmount = new BigDecimal(bpAmount).add(new BigDecimal(line.getWriteOffAmt())).toString();
        }
        if ("".equals(line.getC_GLItem_ID())) {
          fact.createLine(line,
              docPayment.getAccountBPartner(
                  (line.m_C_BPartner_ID == null || line.m_C_BPartner_ID.equals(""))
                      ? docPayment.C_BPartner_ID
                      : line.m_C_BPartner_ID,
                  as, isReceipt, isPrepayment, conn),
              docPayment.C_Currency_ID, (isReceipt ? "" : bpAmount), (isReceipt ? bpAmount : ""),
              Fact_Acct_Group_ID, docPayment.nextSeqNo(docPayment.getSeqNo()),
              docPayment.DocumentType, conn);
        } else {
          fact.createLine(line,
              docPayment.getAccountGLItem(
                  OBDal.getInstance().get(GLItem.class, line.getC_GLItem_ID()), as, isReceipt,
                  conn),
              docPayment.C_Currency_ID, (isReceipt ? "" : bpAmount), (isReceipt ? bpAmount : ""),
              Fact_Acct_Group_ID, docPayment.nextSeqNo(docPayment.getSeqNo()),
              docPayment.DocumentType, conn);
        }
      }
      FIN_Payment payment = OBDal.getInstance().get(FIN_Payment.class, docPayment.Record_ID);
      fact.createLine(null,
          docPayment.getAccount(conn, payment.getPaymentMethod(), payment.getAccount(), as,
              payment.isReceipt()),
          docPayment.C_Currency_ID,
          (payment.isReceipt() ? docPayment.Amounts[AcctServer.AMTTYPE_Gross] : ""),
          (payment.isReceipt() ? "" : docPayment.Amounts[AcctServer.AMTTYPE_Gross]),
          Fact_Acct_Group_ID, "999999", docPayment.DocumentType, conn);
      // Pre-payment is consumed when Used Credit Amount not equals Zero. When consuming Credit no
      // credit is generated
      if (new BigDecimal(docPayment.getUsedAmount()).compareTo(BigDecimal.ZERO) != 0
          && new BigDecimal(docPayment.getGeneratedAmount()).compareTo(BigDecimal.ZERO) == 0) {
        fact.createLine(null,
            docPayment.getAccountBPartner(docPayment.C_BPartner_ID, as, payment.isReceipt(), true,
                conn),
            docPayment.C_Currency_ID, (payment.isReceipt() ? docPayment.getUsedAmount() : ""),
            (payment.isReceipt() ? "" : docPayment.getUsedAmount()), Fact_Acct_Group_ID,
            docPayment.nextSeqNo(docPayment.getSeqNo()), docPayment.DocumentType, conn);
      }

      // Timing of VAT
      if (payment.getOrganization().isObtvbIssettlementvat()) {
        for (final FIN_PaymentDetailV detail : payment.getFINPaymentDetailVList()) {
          Invoice invoice;
          Order order;
          final BigDecimal paidAmt = detail.getPaidAmount();
          try {
            invoice = detail.getPaymentPlanInvoice().getInvoice();
            log4j.debug("Invoice detected: " + invoice.getIdentifier());
            if (!invoice.isSalesTransaction()) {
              invoice = null;
              log4j.debug("purchase invoice, so no timing of vat will be done");
            }
          } catch (final NullPointerException npe) {
            log4j.debug("No invoice detected");
            invoice = null;
          }
          try {
            order = detail.getOrderPaymentPlan().getSalesOrder();
            log4j.debug("Order detected: " + order.getIdentifier());
            if (!order.isSalesTransaction()) {
              order = null;
              log4j.debug("purchase order, so no timing of vat will be done");
            }
          } catch (final NullPointerException npe) {
            log4j.debug("No order detected");
            order = null;
          }

          if (invoice != null) {
            final BigDecimal docTotaAmt = invoice.getGrandTotalAmount();
            final BigDecimal percentage;
            if (docTotaAmt.compareTo(BigDecimal.ZERO) == 0) {
              percentage = BigDecimal.ZERO;
            } else {
              percentage = paidAmt.multiply(new BigDecimal("100"))
                  .divide(docTotaAmt, PERCENTAGE_SCALE, RoundingMode.HALF_UP);
            }
            log4j.debug("Document total amount: " + docTotaAmt);
            log4j.debug("Paid amount: " + paidAmt);
            log4j.debug("Percentage to apply to each tax amount: " + percentage + "%");

            for (final InvoiceTax documentTax : invoice.getInvoiceTaxList()) {
              final TaxRate tax = documentTax.getTax();
              final DocTax docTax = new DocTax(tax.getId(), tax.getName(), tax.getRate().toString(),
                  documentTax.getTaxableAmount().toString(), documentTax.getTaxAmount().toString());
              final BigDecimal taxAcctAmt = documentTax.getTaxAmount()
                  .multiply(percentage)
                  .divide(new BigDecimal("100"), 10, RoundingMode.HALF_UP);

              final String entryAmount = OBTVB_Utility.round(taxAcctAmt, docPayment.C_Currency_ID)
                  .toString();
              log4j.debug("Entry amount for tax (" + tax.getIdentifier() + "): " + entryAmount
                  + " (rounded from " + taxAcctAmt + ")");

              // Enable this for two different entries
              // Fact_Acct_Group_ID = SequenceIdData.getUUID();
              if (invoice.getDocumentType().getDocumentCategory().equals("ARI")
                  || invoice.getDocumentType().getDocumentCategory().equals("ARP")
                  || invoice.getDocumentType().getDocumentCategory().equals("ARI_RM")) {
                fact.createLine(null, docTax.getAccount(DocTax.ACCTTYPE_TaxDue, as, conn),
                    docPayment.C_Currency_ID, "", entryAmount, Fact_Acct_Group_ID,
                    docPayment.nextSeqNo(docPayment.getSeqNo()), docPayment.DocumentType, conn);
                fact.createLine(null, OBTVB_Utility.getTransitoryTaxAccount(tax.getId(), as, conn),
                    docPayment.C_Currency_ID, entryAmount, "", Fact_Acct_Group_ID,
                    docPayment.nextSeqNo(docPayment.getSeqNo()), docPayment.DocumentType, conn);
              } else if (invoice.getDocumentType().getDocumentCategory().equals("ARC")) {
                fact.createLine(null, docTax.getAccount(DocTax.ACCTTYPE_TaxDue, as, conn),
                    docPayment.C_Currency_ID, entryAmount, "", Fact_Acct_Group_ID,
                    docPayment.nextSeqNo(docPayment.getSeqNo()), docPayment.DocumentType, conn);
                fact.createLine(null, OBTVB_Utility.getTransitoryTaxAccount(tax.getId(), as, conn),
                    docPayment.C_Currency_ID, "", entryAmount, Fact_Acct_Group_ID,
                    docPayment.nextSeqNo(docPayment.getSeqNo()), docPayment.DocumentType, conn);
              } else if (invoice.getDocumentType().getDocumentCategory().equals("API")) {
                fact.createLine(null, docTax.getAccount(DocTax.ACCTTYPE_TaxCredit, as, conn),
                    docPayment.C_Currency_ID, entryAmount, "", Fact_Acct_Group_ID,
                    docPayment.nextSeqNo(docPayment.getSeqNo()), docPayment.DocumentType, conn);
                fact.createLine(null, OBTVB_Utility.getTransitoryTaxAccount(tax.getId(), as, conn),
                    docPayment.C_Currency_ID, "", entryAmount, Fact_Acct_Group_ID,
                    docPayment.nextSeqNo(docPayment.getSeqNo()), docPayment.DocumentType, conn);
              } else if (invoice.getDocumentType().getDocumentCategory().equals("APC")) {
                fact.createLine(null, docTax.getAccount(DocTax.ACCTTYPE_TaxCredit, as, conn),
                    docPayment.C_Currency_ID, "", entryAmount, Fact_Acct_Group_ID,
                    docPayment.nextSeqNo(docPayment.getSeqNo()), docPayment.DocumentType, conn);
                fact.createLine(null, OBTVB_Utility.getTransitoryTaxAccount(tax.getId(), as, conn),
                    docPayment.C_Currency_ID, entryAmount, "", Fact_Acct_Group_ID,
                    docPayment.nextSeqNo(docPayment.getSeqNo()), docPayment.DocumentType, conn);
              }
            }

          } else if (order != null) {
            final BigDecimal docTotaAmt = order.getGrandTotalAmount();
            final BigDecimal percentage;
            if (docTotaAmt.compareTo(BigDecimal.ZERO) == 0) {
              percentage = BigDecimal.ZERO;
            } else {
              percentage = paidAmt.multiply(new BigDecimal("100"))
                  .divide(docTotaAmt, PERCENTAGE_SCALE, RoundingMode.HALF_UP);
            }
            log4j.debug("Document total amount: " + docTotaAmt);
            log4j.debug("Paid amount: " + paidAmt);
            log4j.debug("Percentage to apply to each tax amount: " + percentage + "%");

            for (final OrderTax documentTax : order.getOrderTaxList()) {
              final TaxRate tax = documentTax.getTax();
              DocTax docTax = new DocTax(tax.getId(), tax.getName(), tax.getRate().toString(),
                  documentTax.getTaxableAmount().toString(), documentTax.getTaxAmount().toString());
              final BigDecimal taxAcctAmt = documentTax.getTaxAmount()
                  .multiply(percentage)
                  .divide(new BigDecimal("100"), 10, RoundingMode.HALF_UP);

              final String entryAmount = OBTVB_Utility.round(taxAcctAmt, docPayment.C_Currency_ID)
                  .toString();
              log4j.debug("Entry amount for tax (" + tax.getIdentifier() + "): " + entryAmount
                  + " (rounded from " + taxAcctAmt + ")");

              // Enable this for two different entries
              // Fact_Acct_Group_ID = SequenceIdData.getUUID();
              if (order.isSalesTransaction()) {
                fact.createLine(null, docTax.getAccount(DocTax.ACCTTYPE_TaxDue, as, conn),
                    docPayment.C_Currency_ID, "", entryAmount, Fact_Acct_Group_ID,
                    docPayment.nextSeqNo(docPayment.getSeqNo()), docPayment.DocumentType, conn);
                fact.createLine(null, OBTVB_Utility.getTransitoryTaxAccount(tax.getId(), as, conn),
                    docPayment.C_Currency_ID, entryAmount, "", Fact_Acct_Group_ID,
                    docPayment.nextSeqNo(docPayment.getSeqNo()), docPayment.DocumentType, conn);
              } else {
                fact.createLine(null, docTax.getAccount(DocTax.ACCTTYPE_TaxCredit, as, conn),
                    docPayment.C_Currency_ID, entryAmount, "", Fact_Acct_Group_ID,
                    docPayment.nextSeqNo(docPayment.getSeqNo()), docPayment.DocumentType, conn);
                fact.createLine(null, OBTVB_Utility.getTransitoryTaxAccount(tax.getId(), as, conn),
                    docPayment.C_Currency_ID, "", entryAmount, Fact_Acct_Group_ID,
                    docPayment.nextSeqNo(docPayment.getSeqNo()), docPayment.DocumentType, conn);
              }
            }
          } else {
            log4j.error("Neither an order nor an invoice were detected");
          }
        }
      }
    } finally {
      OBContext.restorePreviousMode();
    }

    docPayment.setSeqNo("0");
    return fact;
  }

}

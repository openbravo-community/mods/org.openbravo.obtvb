/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2009-2016 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */
package org.openbravo.obtvb.process;

import java.math.BigDecimal;
import java.sql.Connection;
import java.util.List;

import javax.servlet.ServletException;

import org.apache.log4j.Logger;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.dal.service.OBQuery;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.erpCommon.ad_forms.Account;
import org.openbravo.erpCommon.ad_forms.AcctSchema;
import org.openbravo.erpCommon.ad_forms.AcctServer;
import org.openbravo.erpCommon.ad_forms.DocInvoice;
import org.openbravo.erpCommon.ad_forms.DocInvoiceTemplate;
import org.openbravo.erpCommon.ad_forms.DocLine;
import org.openbravo.erpCommon.ad_forms.DocLine_FinPaymentSchedule;
import org.openbravo.erpCommon.ad_forms.DocLine_Invoice;
import org.openbravo.erpCommon.ad_forms.DocTax;
import org.openbravo.erpCommon.ad_forms.Fact;
import org.openbravo.erpCommon.ad_forms.FactLine;
import org.openbravo.erpCommon.ad_forms.ProductInfo;
import org.openbravo.erpCommon.utility.SequenceIdData;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.model.financialmgmt.tax.TaxRateAccounts;

public class DocInvoiceVatPosting extends DocInvoiceTemplate {

  @SuppressWarnings("unused")
  private static final long serialVersionUID = 1L;
  static Logger log4jDocInvoiceVatPosting = Logger.getLogger(DocInvoiceVatPosting.class);
  String SeqNo = "0";

  public Fact createFact(DocInvoice docInvoice, AcctSchema as, ConnectionProvider conn,
      Connection con, VariablesSecureApp vars) throws ServletException {
    Organization org = null;
    try {
      OBContext.setAdminMode(true);
      org = (Organization) OBDal.getInstance().get(Organization.class, docInvoice.AD_Org_ID);
    } finally {
      OBContext.restorePreviousMode();
    }
    Fact fact = null;
    DocLine_FinPaymentSchedule[] m_payments = docInvoice.getM_payments();

    // Yes: Organization has been configured to Post VAT at the time of settlement.
    // If we do not post VAT in invoice, we should at least post the invoice and instead of using
    // final VAT account (Tax account) we will use transitory VAT account, but in ani case you can
    // not avoid accounting the whole invoice!!!!!
    // No : Organization has not been configured to Post VAT at the time of settlement.
    log4jDocInvoiceVatPosting.debug("Starting create fact");
    // create Fact Header
    fact = new Fact(docInvoice, as, Fact.POST_Actual);
    String Fact_Acct_Group_ID = SequenceIdData.getUUID();
    // Cash based accounting
    if (!as.isAccrual())
      return null;

    /** @todo Assumes TaxIncluded = N */

    // ARI, ARF
    if (docInvoice.DocumentType.equals(AcctServer.DOCTYPE_ARInvoice)
        || docInvoice.DocumentType.equals(AcctServer.DOCTYPE_ARProForma)) {
      log4jDocInvoiceVatPosting.debug("Point 1");
      // Receivables DR
      if (m_payments == null || m_payments.length == 0)
        for (int i = 0; docInvoice.m_debt_payments != null && i < docInvoice.m_debt_payments.length; i++) {
          if (docInvoice.m_debt_payments[i].getIsReceipt().equals("Y"))
            fact.createLine(docInvoice.m_debt_payments[i], docInvoice.getAccountBPartner(
                docInvoice.C_BPartner_ID, as, true, docInvoice.m_debt_payments[i].getDpStatus(),
                conn), docInvoice.C_Currency_ID, AcctServer.getConvertedAmt(
                docInvoice.m_debt_payments[i].getAmount(),
                docInvoice.m_debt_payments[i].getC_Currency_ID_From(), docInvoice.C_Currency_ID,
                docInvoice.DateAcct, "", conn), "", Fact_Acct_Group_ID, nextSeqNo(SeqNo),
                docInvoice.DocumentType, conn);
          else
            fact.createLine(docInvoice.m_debt_payments[i], docInvoice.getAccountBPartner(
                docInvoice.C_BPartner_ID, as, false, docInvoice.m_debt_payments[i].getDpStatus(),
                conn), docInvoice.C_Currency_ID, "", AcctServer.getConvertedAmt(
                docInvoice.m_debt_payments[i].getAmount(),
                docInvoice.m_debt_payments[i].getC_Currency_ID_From(), docInvoice.C_Currency_ID,
                docInvoice.DateAcct, "", conn), Fact_Acct_Group_ID, nextSeqNo(SeqNo),
                docInvoice.DocumentType, conn);
        }
      else
        for (int i = 0; m_payments != null && i < m_payments.length; i++) {
          fact.createLine(m_payments[i], docInvoice.getAccountBPartner(docInvoice.C_BPartner_ID,
              as, true, false, conn), docInvoice.C_Currency_ID, AcctServer.getConvertedAmt(
              m_payments[i].getAmount(), m_payments[i].getC_Currency_ID_From(),
              docInvoice.C_Currency_ID, docInvoice.DateAcct, "", conn), "", Fact_Acct_Group_ID,
              nextSeqNo(SeqNo), docInvoice.DocumentType, conn);
          fact.createLine(m_payments[i], docInvoice.getAccountBPartner(docInvoice.C_BPartner_ID,
              as, true, true, conn), docInvoice.C_Currency_ID, AcctServer.getConvertedAmt(
              m_payments[i].getPrepaidAmount(), m_payments[i].getC_Currency_ID_From(),
              docInvoice.C_Currency_ID, docInvoice.DateAcct, "", conn), "", Fact_Acct_Group_ID,
              nextSeqNo(SeqNo), docInvoice.DocumentType, conn);
        }
      // Charge CR
      log4jDocInvoiceVatPosting.debug("The first create line");
      fact.createLine(null, docInvoice.getAccount(AcctServer.ACCTTYPE_Charge, as, conn),
          docInvoice.C_Currency_ID, "", docInvoice.getAmount(AcctServer.AMTTYPE_Charge),
          Fact_Acct_Group_ID, nextSeqNo(SeqNo), docInvoice.DocumentType, conn);
      // TaxDue CR
      log4jDocInvoiceVatPosting.debug("m_taxes.length: " + docInvoice.getM_taxes());
      for (int i = 0; docInvoice.getM_taxes() != null && i < docInvoice.getM_taxes().length; i++) {
        // New docLine created to assign C_Tax_ID value to the entry
        DocLine docLine = new DocLine(docInvoice.DocumentType, docInvoice.Record_ID, "");
        docLine.m_C_Tax_ID = docInvoice.getM_taxes()[i].m_C_Tax_ID;
        fact.createLine(
            docLine,
            org.isObtvbIssettlementvat() ? getTransitoryTaxAccount(
                docInvoice.getM_taxes()[i].m_C_Tax_ID, as, conn) : docInvoice.getM_taxes()[i]
                .getAccount(DocTax.ACCTTYPE_TaxDue, as, conn), docInvoice.C_Currency_ID, "",
            docInvoice.getM_taxes()[i].m_amount, Fact_Acct_Group_ID, nextSeqNo(SeqNo),
            docInvoice.DocumentType, conn);
      }
      // Revenue CR
      if (docInvoice.p_lines != null && docInvoice.p_lines.length > 0) {
        for (int i = 0; i < docInvoice.p_lines.length; i++)
          fact.createLine(docInvoice.p_lines[i], ((DocLine_Invoice) docInvoice.p_lines[i])
              .getAccount(ProductInfo.ACCTTYPE_P_Revenue, as, conn), docInvoice.C_Currency_ID, "",
              docInvoice.p_lines[i].getAmount(), Fact_Acct_Group_ID, nextSeqNo(SeqNo),
              docInvoice.DocumentType, conn);
      }
      // Set Locations
      FactLine[] fLines = fact.getLines();
      for (int i = 0; i < fLines.length; i++) {
        if (fLines[i] != null) {
          fLines[i].setLocationFromOrg(fLines[i].getAD_Org_ID(conn), true, conn); // from Loc
          fLines[i].setLocationFromBPartner(docInvoice.C_BPartner_Location_ID, false, conn); // to
          // Loc
        }
      }
    }
    // ARC
    else if (docInvoice.DocumentType.equals(AcctServer.DOCTYPE_ARCredit)) {
      log4jDocInvoiceVatPosting.debug("Point 2");
      // Receivables CR
      if (m_payments == null || m_payments.length == 0)
        for (int i = 0; docInvoice.m_debt_payments != null && i < docInvoice.m_debt_payments.length; i++) {
          BigDecimal amount = new BigDecimal(docInvoice.m_debt_payments[i].getAmount());
          fact.createLine(docInvoice.m_debt_payments[i],
              docInvoice.getAccountBPartner(docInvoice.C_BPartner_ID, as, true,
                  docInvoice.m_debt_payments[i].getDpStatus(), conn), docInvoice.C_Currency_ID, "",
              AcctServer.getConvertedAmt(((amount.negate())).toPlainString(),
                  docInvoice.m_debt_payments[i].getC_Currency_ID_From(), docInvoice.C_Currency_ID,
                  docInvoice.DateAcct, "", conn), Fact_Acct_Group_ID, nextSeqNo(SeqNo),
              docInvoice.DocumentType, conn);
        }
      else
        for (int i = 0; m_payments != null && i < m_payments.length; i++) {
          BigDecimal amount = new BigDecimal(m_payments[i].getAmount());
          BigDecimal prepaidAmount = new BigDecimal(m_payments[i].getPrepaidAmount());
          fact.createLine(m_payments[i], docInvoice.getAccountBPartner(docInvoice.C_BPartner_ID,
              as, true, false, conn), docInvoice.C_Currency_ID, "", AcctServer.getConvertedAmt(
              amount.negate().toString(), m_payments[i].getC_Currency_ID_From(),
              docInvoice.C_Currency_ID, docInvoice.DateAcct, "", conn), Fact_Acct_Group_ID,
              nextSeqNo(SeqNo), docInvoice.DocumentType, conn);
          // Pre-payment: Probably not needed as at this point we can not generate pre-payments
          // against ARC. Amount is negated
          fact.createLine(m_payments[i], docInvoice.getAccountBPartner(docInvoice.C_BPartner_ID,
              as, true, true, conn), docInvoice.C_Currency_ID, "", AcctServer.getConvertedAmt(
              prepaidAmount.negate().toString(), m_payments[i].getC_Currency_ID_From(),
              docInvoice.C_Currency_ID, docInvoice.DateAcct, "", conn), Fact_Acct_Group_ID,
              nextSeqNo(SeqNo), docInvoice.DocumentType, conn);
        }

      // fact.createLine(null,
      // getAccount(AcctServer.ACCTTYPE_C_Receivable, as,
      // conn),this.C_Currency_ID, "",
      // getAmount(AcctServer.AMTTYPE_Gross), Fact_Acct_Group_ID,
      // nextSeqNo(SeqNo), DocumentType,conn);
      // Charge DR
      fact.createLine(null, docInvoice.getAccount(AcctServer.ACCTTYPE_Charge, as, conn),
          docInvoice.C_Currency_ID, docInvoice.getAmount(AcctServer.AMTTYPE_Charge), "",
          Fact_Acct_Group_ID, nextSeqNo(SeqNo), docInvoice.DocumentType, conn);
      // TaxDue DR
      for (int i = 0; docInvoice.getM_taxes() != null && i < docInvoice.getM_taxes().length; i++) {
        // New docLine created to assign C_Tax_ID value to the entry
        DocLine docLine = new DocLine(docInvoice.DocumentType, docInvoice.Record_ID, "");
        docLine.m_C_Tax_ID = docInvoice.getM_taxes()[i].m_C_Tax_ID;
        fact.createLine(
            docLine,
            org.isObtvbIssettlementvat() ? getTransitoryTaxAccount(
                docInvoice.getM_taxes()[i].m_C_Tax_ID, as, conn) : docInvoice.getM_taxes()[i]
                .getAccount(DocTax.ACCTTYPE_TaxDue, as, conn), docInvoice.C_Currency_ID, docInvoice
                .getM_taxes()[i].getAmount(), "", Fact_Acct_Group_ID, nextSeqNo(SeqNo),
            docInvoice.DocumentType, conn);
      }
      // Revenue CR
      for (int i = 0; docInvoice.p_lines != null && i < docInvoice.p_lines.length; i++)
        fact.createLine(docInvoice.p_lines[i], ((DocLine_Invoice) docInvoice.p_lines[i])
            .getAccount(ProductInfo.ACCTTYPE_P_Revenue, as, conn), docInvoice.C_Currency_ID,
            docInvoice.p_lines[i].getAmount(), "", Fact_Acct_Group_ID, nextSeqNo(SeqNo),
            docInvoice.DocumentType, conn);
      // Set Locations
      FactLine[] fLines = fact.getLines();
      for (int i = 0; fLines != null && i < fLines.length; i++) {
        if (fLines[i] != null) {
          fLines[i].setLocationFromOrg(fLines[i].getAD_Org_ID(conn), true, conn); // from Loc
          fLines[i].setLocationFromBPartner(docInvoice.C_BPartner_Location_ID, false, conn); // to
          // Loc
        }
      }
    }
    // API
    else if (docInvoice.DocumentType.equals(AcctServer.DOCTYPE_APInvoice)) {
      log4jDocInvoiceVatPosting.debug("Point 3");
      // Liability CR
      if (m_payments == null || m_payments.length == 0)
        for (int i = 0; docInvoice.m_debt_payments != null && i < docInvoice.m_debt_payments.length; i++) {
          if (docInvoice.m_debt_payments[i].getIsReceipt().equals("Y"))
            fact.createLine(docInvoice.m_debt_payments[i], docInvoice.getAccountBPartner(
                docInvoice.C_BPartner_ID, as, true, docInvoice.m_debt_payments[i].getDpStatus(),
                conn), docInvoice.C_Currency_ID, AcctServer.getConvertedAmt(
                docInvoice.m_debt_payments[i].getAmount(),
                docInvoice.m_debt_payments[i].getC_Currency_ID_From(), docInvoice.C_Currency_ID,
                docInvoice.DateAcct, "", conn), "", Fact_Acct_Group_ID, nextSeqNo(SeqNo),
                docInvoice.DocumentType, conn);
          else
            fact.createLine(docInvoice.m_debt_payments[i], docInvoice.getAccountBPartner(
                docInvoice.C_BPartner_ID, as, false, docInvoice.m_debt_payments[i].getDpStatus(),
                conn), docInvoice.C_Currency_ID, "", AcctServer.getConvertedAmt(
                docInvoice.m_debt_payments[i].getAmount(),
                docInvoice.m_debt_payments[i].getC_Currency_ID_From(), docInvoice.C_Currency_ID,
                docInvoice.DateAcct, "", conn), Fact_Acct_Group_ID, nextSeqNo(SeqNo),
                docInvoice.DocumentType, conn);
        }

      else
        for (int i = 0; m_payments != null && i < m_payments.length; i++) {
          fact.createLine(m_payments[i], docInvoice.getAccountBPartner(docInvoice.C_BPartner_ID,
              as, false, false, conn), docInvoice.C_Currency_ID, "", AcctServer.getConvertedAmt(
              m_payments[i].getAmount(), m_payments[i].getC_Currency_ID_From(),
              docInvoice.C_Currency_ID, docInvoice.DateAcct, "", conn), Fact_Acct_Group_ID,
              nextSeqNo(SeqNo), docInvoice.DocumentType, conn);
          fact.createLine(m_payments[i], docInvoice.getAccountBPartner(docInvoice.C_BPartner_ID,
              as, false, true, conn), docInvoice.C_Currency_ID, "", AcctServer.getConvertedAmt(
              m_payments[i].getPrepaidAmount(), m_payments[i].getC_Currency_ID_From(),
              docInvoice.C_Currency_ID, docInvoice.DateAcct, "", conn), Fact_Acct_Group_ID,
              nextSeqNo(SeqNo), docInvoice.DocumentType, conn);
        }
      // fact.createLine(null, getAccount(AcctServer.ACCTTYPE_V_Liability,
      // as, conn),this.C_Currency_ID, "",
      // getAmount(AcctServer.AMTTYPE_Gross), Fact_Acct_Group_ID,
      // nextSeqNo(SeqNo), DocumentType,conn);
      // Charge DR
      fact.createLine(null, docInvoice.getAccount(AcctServer.ACCTTYPE_Charge, as, conn),
          docInvoice.C_Currency_ID, docInvoice.getAmount(AcctServer.AMTTYPE_Charge), "",
          Fact_Acct_Group_ID, nextSeqNo(SeqNo), docInvoice.DocumentType, conn);
      // TaxCredit DR
      for (int i = 0; docInvoice.getM_taxes() != null && i < docInvoice.getM_taxes().length; i++) {
        // New docLine created to assign C_Tax_ID value to the entry
        DocLine docLine = new DocLine(docInvoice.DocumentType, docInvoice.Record_ID, "");
        docLine.m_C_Tax_ID = docInvoice.getM_taxes()[i].m_C_Tax_ID;
        if (docInvoice.getM_taxes()[i].m_isTaxUndeductable)
          computeTaxUndeductableLine(conn, as, fact, docLine, Fact_Acct_Group_ID,
              docInvoice.getM_taxes()[i].m_C_Tax_ID, docInvoice.getM_taxes()[i].getAmount(),
              docInvoice);
        else
          fact.createLine(docLine,
              docInvoice.getM_taxes()[i].getAccount(DocTax.ACCTTYPE_TaxCredit, as, conn),
              docInvoice.C_Currency_ID, docInvoice.getM_taxes()[i].getAmount(), "",
              Fact_Acct_Group_ID, nextSeqNo(SeqNo), docInvoice.DocumentType, conn);
      }
      // Expense DR
      for (int i = 0; docInvoice.p_lines != null && i < docInvoice.p_lines.length; i++)
        fact.createLine(docInvoice.p_lines[i], ((DocLine_Invoice) docInvoice.p_lines[i])
            .getAccount(ProductInfo.ACCTTYPE_P_Expense, as, conn), docInvoice.C_Currency_ID,
            docInvoice.p_lines[i].getAmount(), "", Fact_Acct_Group_ID, nextSeqNo(SeqNo),
            docInvoice.DocumentType, conn);
      // Set Locations
      FactLine[] fLines = fact.getLines();
      for (int i = 0; fLines != null && i < fLines.length; i++) {
        if (fLines[i] != null) {
          fLines[i].setLocationFromBPartner(docInvoice.C_BPartner_Location_ID, true, conn); // from
          // Loc
          fLines[i].setLocationFromOrg(fLines[i].getAD_Org_ID(conn), false, conn); // to Loc
        }
      }
      docInvoice.updateProductInfo(as.getC_AcctSchema_ID(), conn, con); // only API
    }
    // APC
    else if (docInvoice.DocumentType.equals(AcctServer.DOCTYPE_APCredit)) {
      log4jDocInvoiceVatPosting.debug("Point 4");
      // Liability DR
      if (m_payments == null || m_payments.length == 0)
        for (int i = 0; docInvoice.m_debt_payments != null && i < docInvoice.m_debt_payments.length; i++) {
          BigDecimal amount = new BigDecimal(docInvoice.m_debt_payments[i].getAmount());
          fact.createLine(docInvoice.m_debt_payments[i], docInvoice.getAccountBPartner(
              docInvoice.C_BPartner_ID, as, false, docInvoice.m_debt_payments[i].getDpStatus(),
              conn), docInvoice.C_Currency_ID, AcctServer.getConvertedAmt(
              ((amount.negate())).toPlainString(),
              docInvoice.m_debt_payments[i].getC_Currency_ID_From(), docInvoice.C_Currency_ID,
              docInvoice.DateAcct, "", conn), "", Fact_Acct_Group_ID, nextSeqNo(SeqNo),
              docInvoice.DocumentType, conn);
        }
      else
        for (int i = 0; m_payments != null && i < m_payments.length; i++) {
          BigDecimal amount = new BigDecimal(m_payments[i].getAmount());
          BigDecimal prepaidAmount = new BigDecimal(m_payments[i].getPrepaidAmount());
          fact.createLine(m_payments[i], docInvoice.getAccountBPartner(docInvoice.C_BPartner_ID,
              as, false, false, conn), docInvoice.C_Currency_ID, AcctServer.getConvertedAmt(amount
              .negate().toString(), m_payments[i].getC_Currency_ID_From(),
              docInvoice.C_Currency_ID, docInvoice.DateAcct, "", conn), "", Fact_Acct_Group_ID,
              nextSeqNo(SeqNo), docInvoice.DocumentType, conn);
          // Pre-payment: Probably not needed as at this point we can not generate pre-payments
          // against APC. Amount is negated
          fact.createLine(m_payments[i], docInvoice.getAccountBPartner(docInvoice.C_BPartner_ID,
              as, false, true, conn), docInvoice.C_Currency_ID, AcctServer.getConvertedAmt(
              prepaidAmount.negate().toString(), m_payments[i].getC_Currency_ID_From(),
              docInvoice.C_Currency_ID, docInvoice.DateAcct, "", conn), "", Fact_Acct_Group_ID,
              nextSeqNo(SeqNo), docInvoice.DocumentType, conn);

        }

      // fact.createLine (null,
      // getAccount(AcctServer.ACCTTYPE_V_Liability, as,
      // conn),this.C_Currency_ID,"", getAmount(AcctServer.AMTTYPE_Gross),
      // Fact_Acct_Group_ID, nextSeqNo(SeqNo), DocumentType,conn);
      // Charge CR
      fact.createLine(null, docInvoice.getAccount(AcctServer.ACCTTYPE_Charge, as, conn),
          docInvoice.C_Currency_ID, "", docInvoice.getAmount(AcctServer.AMTTYPE_Charge),
          Fact_Acct_Group_ID, nextSeqNo(SeqNo), docInvoice.DocumentType, conn);
      // TaxCredit CR
      for (int i = 0; docInvoice.getM_taxes() != null && i < docInvoice.getM_taxes().length; i++) {
        // New docLine created to assign C_Tax_ID value to the entry
        DocLine docLine = new DocLine(docInvoice.DocumentType, docInvoice.Record_ID, "");
        docLine.m_C_Tax_ID = docInvoice.getM_taxes()[i].m_C_Tax_ID;
        if (docInvoice.getM_taxes()[i].m_isTaxUndeductable)
          computeTaxUndeductableLine(conn, as, fact, docLine, Fact_Acct_Group_ID,
              docInvoice.getM_taxes()[i].m_C_Tax_ID, docInvoice.getM_taxes()[i].getAmount(),
              docInvoice);

        else
          fact.createLine(docLine,
              docInvoice.getM_taxes()[i].getAccount(DocTax.ACCTTYPE_TaxCredit, as, conn),
              docInvoice.C_Currency_ID, "", docInvoice.getM_taxes()[i].getAmount(),
              Fact_Acct_Group_ID, nextSeqNo(SeqNo), docInvoice.DocumentType, conn);
      }
      // Expense CR
      for (int i = 0; docInvoice.p_lines != null && i < docInvoice.p_lines.length; i++)
        fact.createLine(docInvoice.p_lines[i], ((DocLine_Invoice) docInvoice.p_lines[i])
            .getAccount(ProductInfo.ACCTTYPE_P_Expense, as, conn), docInvoice.C_Currency_ID, "",
            docInvoice.p_lines[i].getAmount(), Fact_Acct_Group_ID, nextSeqNo(SeqNo),
            docInvoice.DocumentType, conn);
      // Set Locations
      FactLine[] fLines = fact.getLines();
      for (int i = 0; fLines != null && i < fLines.length; i++) {
        if (fLines[i] != null) {
          fLines[i].setLocationFromBPartner(docInvoice.C_BPartner_Location_ID, true, conn); // from
          // Loc
          fLines[i].setLocationFromOrg(fLines[i].getAD_Org_ID(conn), false, conn); // to Loc
        }
      }
    } else {
      log4jDocInvoiceVatPosting.warn("Doc_Invoice - DocumentType unknown: "
          + docInvoice.DocumentType);
      fact = null;
    }
    SeqNo = "0";

    return fact;
  }

  /**
   * getTransitoryTaxAccount
   * 
   * @param strcTaxId
   * @param as
   * @param conn
   * @return
   */
  public Account getTransitoryTaxAccount(String strcTaxId, AcctSchema as, ConnectionProvider conn) {
    Account transitoryAccount = null;
    try {
      OBContext.setAdminMode(true);
      final StringBuilder whereClause = new StringBuilder();

      if (!strcTaxId.equals("")) {
        whereClause.append(" as tra ");
        whereClause.append(" where tra.tax.id = '" + strcTaxId + "'");
        whereClause.append(" and tra.accountingSchema.id = '" + as.m_C_AcctSchema_ID + "'");
      } else
        return null;

      final OBQuery<TaxRateAccounts> obqParameters = OBDal.getInstance().createQuery(
          TaxRateAccounts.class, whereClause.toString());
      // For Background process execution
      obqParameters.setFilterOnReadableClients(false);
      obqParameters.setFilterOnReadableOrganization(false);
      final List<TaxRateAccounts> taxRateAccounts = obqParameters.list();
      transitoryAccount = new Account(conn, taxRateAccounts.get(0).getObtvbTTransitoryAcct()
          .getId());
    } catch (ServletException e) {
      log4jDocInvoiceVatPosting.warn(e);
    } finally {
      OBContext.restorePreviousMode();
    }
    return transitoryAccount;
  } // getTransitoryTaxAccount

  /**
   * 
   * @param oldSeqNo
   * @return
   */
  public String nextSeqNo(String oldSeqNo) {
    log4jDocInvoiceVatPosting.debug("DocInvoice - oldSeqNo = " + oldSeqNo);
    BigDecimal seqNo = new BigDecimal(oldSeqNo);
    SeqNo = (seqNo.add(new BigDecimal("10"))).toString();
    log4jDocInvoiceVatPosting.debug("DocInvoice - nextSeqNo = " + SeqNo);
    return SeqNo;
  }

  private void computeTaxUndeductableLine(ConnectionProvider conn, AcctSchema as, Fact fact,
      DocLine docLine, String Fact_Acct_Group_ID, String taxId, String strTaxAmount,
      DocInvoice docInvoice) {
    int invoiceLineTaxCount = 0;
    int totalInvoiceLineTax = getTaxLineCount(conn, taxId, docInvoice);
    BigDecimal cumulativeTaxLineAmount = new BigDecimal(0);
    BigDecimal taxAmount = new BigDecimal(strTaxAmount.equals("") ? "0.00" : strTaxAmount);
    DocInvoiceVatPostingData[] data = null;
    try {
      // We can have some lines from product or some lines from general ledger
      data = DocInvoiceVatPostingData.selectProductAcct(conn, as.getC_AcctSchema_ID(), taxId,
          docInvoice.Record_ID);
      cumulativeTaxLineAmount = createLineForTaxUndeductable(invoiceLineTaxCount,
          totalInvoiceLineTax, cumulativeTaxLineAmount, taxAmount, data, conn, fact, docLine,
          Fact_Acct_Group_ID, docInvoice);
      invoiceLineTaxCount = data.length;
      // check whether gl item is selected instead of product in invoice line
      data = DocInvoiceVatPostingData.selectGLItemAcctForTaxLine(conn, as.getC_AcctSchema_ID(),
          taxId, docInvoice.Record_ID);
      createLineForTaxUndeductable(invoiceLineTaxCount, totalInvoiceLineTax,
          cumulativeTaxLineAmount, taxAmount, data, conn, fact, docLine, Fact_Acct_Group_ID,
          docInvoice);
    } catch (ServletException e) {
      log4jDocInvoiceVatPosting.error("Exception in computeTaxUndeductableLine method: " + e);
    }
  }

  private int getTaxLineCount(ConnectionProvider conn, String taxId, DocInvoice docInvoice) {
    DocInvoiceVatPostingData[] data = null;
    try {
      data = DocInvoiceVatPostingData.getTaxLineCount(conn, taxId, docInvoice.Record_ID);
    } catch (ServletException e) {
      log4jDocInvoiceVatPosting.error("Exception in getTaxLineCount method: " + e);
    }
    if (data.length > 0) {
      return Integer.parseInt(data[0].totallines);
    }
    return 0;
  }

  private BigDecimal createLineForTaxUndeductable(int invoiceLineTaxCount_param,
      int totalInvoiceLineTax, BigDecimal cumulativeTaxLineAmount_param, BigDecimal taxAmount,
      DocInvoiceVatPostingData[] data, ConnectionProvider conn, Fact fact, DocLine docLine,
      String Fact_Acct_Group_ID, DocInvoice docInvoice) {
    BigDecimal cumulativeTaxLineAmount = cumulativeTaxLineAmount_param;
    int invoiceLineTaxCount = invoiceLineTaxCount_param;
    for (int j = 0; j < data.length; j++) {
      invoiceLineTaxCount++;
      // We have to adjust the amount in last line of tax
      if (invoiceLineTaxCount == totalInvoiceLineTax) {
        data[j].taxamt = taxAmount.subtract(cumulativeTaxLineAmount).toPlainString();
      }
      try {
        // currently applicable for API and APC
        if (docInvoice.DocumentType.equals(AcctServer.DOCTYPE_APInvoice)) {
          fact.createLine(docLine, Account.getAccount(conn, data[j].pExpenseAcct),
              docInvoice.C_Currency_ID, data[j].taxamt, "", Fact_Acct_Group_ID, nextSeqNo(SeqNo),
              docInvoice.DocumentType, conn);
        } else if (docInvoice.DocumentType.equals(AcctServer.DOCTYPE_APCredit)) {
          fact.createLine(docLine, Account.getAccount(conn, data[j].pExpenseAcct),
              docInvoice.C_Currency_ID, "", data[j].taxamt, Fact_Acct_Group_ID, nextSeqNo(SeqNo),
              docInvoice.DocumentType, conn);
        }
        cumulativeTaxLineAmount = cumulativeTaxLineAmount.add(new BigDecimal(data[j].taxamt));
      } catch (ServletException e) {
        log4jDocInvoiceVatPosting.error("Exception in createLineForTaxUndeductable method: " + e);
      }
    }
    return cumulativeTaxLineAmount;
  }

}

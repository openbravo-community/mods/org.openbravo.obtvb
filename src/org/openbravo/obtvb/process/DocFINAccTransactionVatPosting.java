/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2011-2020 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */

package org.openbravo.obtvb.process;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Connection;

import javax.servlet.ServletException;

import org.apache.log4j.Logger;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.erpCommon.ad_forms.AcctSchema;
import org.openbravo.erpCommon.ad_forms.AcctServer;
import org.openbravo.erpCommon.ad_forms.DocFINFinAccTransaction;
import org.openbravo.erpCommon.ad_forms.DocFINFinAccTransactionTemplate;
import org.openbravo.erpCommon.ad_forms.DocLine_FINFinAccTransaction;
import org.openbravo.erpCommon.ad_forms.DocTax;
import org.openbravo.erpCommon.ad_forms.Fact;
import org.openbravo.erpCommon.utility.SequenceIdData;
import org.openbravo.model.common.invoice.Invoice;
import org.openbravo.model.common.invoice.InvoiceTax;
import org.openbravo.model.common.order.Order;
import org.openbravo.model.common.order.OrderTax;
import org.openbravo.model.financialmgmt.payment.FIN_FinaccTransaction;
import org.openbravo.model.financialmgmt.payment.FIN_Payment;
import org.openbravo.model.financialmgmt.payment.FIN_PaymentDetailV;
import org.openbravo.model.financialmgmt.tax.TaxRate;
import org.openbravo.obtvb.utillity.OBTVB_Utility;

public class DocFINAccTransactionVatPosting extends DocFINFinAccTransactionTemplate {
  static Logger log4j = Logger.getLogger(DocFINAccTransactionVatPosting.class);
  private static final int PERCENTAGE_SCALE = 10;
  String SeqNo = "0";

  public String getSeqNo() {
    return SeqNo;
  }

  @SuppressWarnings("static-access")
  @Override
  public Fact createFact(DocFINFinAccTransaction docFAT, AcctSchema as, ConnectionProvider conn,
      Connection con, VariablesSecureApp vars) throws ServletException {
    try {
      OBContext.setAdminMode();
      Fact fact = new Fact(docFAT, as, Fact.POST_Actual);
      FIN_FinaccTransaction transaction = OBDal.getInstance()
          .get(FIN_FinaccTransaction.class, docFAT.Record_ID);
      // 3 Scenarios: 1st Bank fee 2nd payment related transaction 3rd glitem transaction
      if (docFAT.TRXTYPE_BankFee.equals(transaction.getTransactionType())) {
        fact = docFAT.createFactFee(transaction, as, conn, fact);
      } else if (transaction.getFinPayment() != null) {
        fact = createFactPaymentDetails(docFAT, as, conn, fact);
      } else {
        fact = docFAT.createFactGLItem(as, conn, fact);
      }

      return fact;
    } finally {
      OBContext.restorePreviousMode();
    }
  }

  public Fact createFactPaymentDetails(DocFINFinAccTransaction docFAT, AcctSchema as,
      ConnectionProvider conn, Fact fact) throws ServletException {
    FIN_FinaccTransaction transaction = OBDal.getInstance()
        .get(FIN_FinaccTransaction.class, docFAT.Record_ID);
    String Fact_Acct_Group_ID = SequenceIdData.getUUID();
    if (!docFAT.getDocumentPaymentConfirmation(transaction.getFinPayment())) {
      for (int i = 0; docFAT.p_lines != null && i < docFAT.p_lines.length; i++) {
        DocLine_FINFinAccTransaction line = (DocLine_FINFinAccTransaction) docFAT.p_lines[i];
        boolean isPrepayment = "Y".equals(line.getIsPrepayment());
        boolean isReceipt = transaction.getFinPayment().isReceipt();
        BigDecimal bpamount = new BigDecimal(line.getAmount());
        if (!"".equals(line.getWriteOffAmt())
            && BigDecimal.ZERO.compareTo(new BigDecimal(line.getWriteOffAmt())) != 0) {
          fact.createLine(line, docFAT.getAccount(AcctServer.ACCTTYPE_WriteOffDefault, as, conn),
              docFAT.C_Currency_ID, (isReceipt ? line.getWriteOffAmt() : ""),
              (isReceipt ? "" : line.getWriteOffAmt()), Fact_Acct_Group_ID, docFAT.nextSeqNo(SeqNo),
              docFAT.DocumentType, conn);
          bpamount = bpamount.add(new BigDecimal(line.getWriteOffAmt()));
        }
        fact.createLine(line, docFAT.getAccountBPartner(
            (line.m_C_BPartner_ID == null || line.m_C_BPartner_ID.equals("")) ? docFAT.C_BPartner_ID
                : line.m_C_BPartner_ID,
            as, isReceipt, isPrepayment, conn), docFAT.C_Currency_ID,
            !isReceipt ? bpamount.toString() : "", isReceipt ? bpamount.toString() : "",
            Fact_Acct_Group_ID, docFAT.nextSeqNo(SeqNo), docFAT.DocumentType, conn);
      }
      // Pre-payment is consumed when Used Credit Amount not equals Zero. When consuming Credit no
      // credit is generated
      if (transaction.getFinPayment().getUsedCredit().compareTo(BigDecimal.ZERO) != 0
          && transaction.getFinPayment().getGeneratedCredit().compareTo(BigDecimal.ZERO) == 0) {
        fact.createLine(null,
            docFAT.getAccountBPartner(
                docFAT.C_BPartner_ID, as, transaction.getFinPayment().isReceipt(), true, conn),
            docFAT.C_Currency_ID,
            (transaction.getFinPayment().isReceipt()
                ? transaction.getFinPayment().getUsedCredit().toString()
                : ""),
            (transaction.getFinPayment().isReceipt() ? ""
                : transaction.getFinPayment().getUsedCredit().toString()),
            Fact_Acct_Group_ID, docFAT.nextSeqNo(SeqNo), docFAT.DocumentType, conn);
      }

      // Timing of VAT
      FIN_Payment payment = transaction.getFinPayment();
      if (payment.getOrganization().isObtvbIssettlementvat()) {
        for (final FIN_PaymentDetailV detail : payment.getFINPaymentDetailVList()) {
          Invoice invoice;
          Order order;
          final BigDecimal paidAmt = detail.getPaidAmount();
          try {
            invoice = detail.getPaymentPlanInvoice().getInvoice();
            log4j.debug("Invoice detected: " + invoice.getIdentifier());
            if (!invoice.isSalesTransaction()) {
              invoice = null;
              log4j.debug("purchase invoice, so no timing of vat will be done");
            }
          } catch (final NullPointerException npe) {
            log4j.debug("No invoice detected");
            invoice = null;
          }
          try {
            order = detail.getOrderPaymentPlan().getSalesOrder();
            log4j.debug("Order detected: " + order.getIdentifier());
            if (!order.isSalesTransaction()) {
              order = null;
              log4j.debug("purchase order, so no timing of vat will be done");
            }
          } catch (final NullPointerException npe) {
            log4j.debug("No order detected");
            order = null;
          }

          if (invoice != null) {
            final BigDecimal docTotaAmt = invoice.getGrandTotalAmount();
            final BigDecimal percentage;
            if (docTotaAmt.compareTo(BigDecimal.ZERO) == 0) {
              percentage = BigDecimal.ZERO;
            } else {
              percentage = paidAmt.multiply(new BigDecimal("100"))
                  .divide(docTotaAmt, PERCENTAGE_SCALE, RoundingMode.HALF_UP);
            }
            log4j.debug("Document total amount: " + docTotaAmt);
            log4j.debug("Paid amount: " + paidAmt);
            log4j.debug("Percentage to apply to each tax amount: " + percentage + "%");

            for (final InvoiceTax documentTax : invoice.getInvoiceTaxList()) {
              final TaxRate tax = documentTax.getTax();
              final DocTax docTax = new DocTax(tax.getId(), tax.getName(), tax.getRate().toString(),
                  documentTax.getTaxableAmount().toString(), documentTax.getTaxAmount().toString());
              final BigDecimal taxAcctAmt = documentTax.getTaxAmount()
                  .multiply(percentage)
                  .divide(new BigDecimal("100"), 10, RoundingMode.HALF_UP);

              final String entryAmount = OBTVB_Utility.round(taxAcctAmt, docFAT.C_Currency_ID)
                  .toString();
              log4j.debug("Entry amount for tax (" + tax.getIdentifier() + "): " + entryAmount
                  + " (rounded from " + taxAcctAmt + ")");

              // Enable this for two different entries
              // Fact_Acct_Group_ID = SequenceIdData.getUUID();
              if (invoice.getDocumentType().getDocumentCategory().equals("ARI")
                  || invoice.getDocumentType().getDocumentCategory().equals("ARP")
                  || invoice.getDocumentType().getDocumentCategory().equals("ARI_RM")) {
                fact.createLine(null, docTax.getAccount(DocTax.ACCTTYPE_TaxDue, as, conn),
                    docFAT.C_Currency_ID, "", entryAmount, Fact_Acct_Group_ID,
                    docFAT.nextSeqNo(getSeqNo()), docFAT.DocumentType, conn);
                fact.createLine(null, OBTVB_Utility.getTransitoryTaxAccount(tax.getId(), as, conn),
                    docFAT.C_Currency_ID, entryAmount, "", Fact_Acct_Group_ID,
                    docFAT.nextSeqNo(getSeqNo()), docFAT.DocumentType, conn);
              } else if (invoice.getDocumentType().getDocumentCategory().equals("ARC")) {
                fact.createLine(null, docTax.getAccount(DocTax.ACCTTYPE_TaxDue, as, conn),
                    docFAT.C_Currency_ID, entryAmount, "", Fact_Acct_Group_ID,
                    docFAT.nextSeqNo(getSeqNo()), docFAT.DocumentType, conn);
                fact.createLine(null, OBTVB_Utility.getTransitoryTaxAccount(tax.getId(), as, conn),
                    docFAT.C_Currency_ID, "", entryAmount, Fact_Acct_Group_ID,
                    docFAT.nextSeqNo(getSeqNo()), docFAT.DocumentType, conn);
              } else if (invoice.getDocumentType().getDocumentCategory().equals("API")) {
                fact.createLine(null, docTax.getAccount(DocTax.ACCTTYPE_TaxCredit, as, conn),
                    docFAT.C_Currency_ID, entryAmount, "", Fact_Acct_Group_ID,
                    docFAT.nextSeqNo(getSeqNo()), docFAT.DocumentType, conn);
                fact.createLine(null, OBTVB_Utility.getTransitoryTaxAccount(tax.getId(), as, conn),
                    docFAT.C_Currency_ID, "", entryAmount, Fact_Acct_Group_ID,
                    docFAT.nextSeqNo(getSeqNo()), docFAT.DocumentType, conn);
              } else if (invoice.getDocumentType().getDocumentCategory().equals("APC")) {
                fact.createLine(null, docTax.getAccount(DocTax.ACCTTYPE_TaxCredit, as, conn),
                    docFAT.C_Currency_ID, "", entryAmount, Fact_Acct_Group_ID,
                    docFAT.nextSeqNo(getSeqNo()), docFAT.DocumentType, conn);
                fact.createLine(null, OBTVB_Utility.getTransitoryTaxAccount(tax.getId(), as, conn),
                    docFAT.C_Currency_ID, entryAmount, "", Fact_Acct_Group_ID,
                    docFAT.nextSeqNo(getSeqNo()), docFAT.DocumentType, conn);
              }
            }

          } else if (order != null) {
            final BigDecimal docTotaAmt = order.getGrandTotalAmount();
            final BigDecimal percentage;
            if (docTotaAmt.compareTo(BigDecimal.ZERO) == 0) {
              percentage = BigDecimal.ZERO;
            } else {
              percentage = paidAmt.multiply(new BigDecimal("100"))
                  .divide(docTotaAmt, PERCENTAGE_SCALE, RoundingMode.HALF_UP);
            }
            log4j.debug("Document total amount: " + docTotaAmt);
            log4j.debug("Paid amount: " + paidAmt);
            log4j.debug("Percentage to apply to each tax amount: " + percentage + "%");

            for (final OrderTax documentTax : order.getOrderTaxList()) {
              final TaxRate tax = documentTax.getTax();
              DocTax docTax = new DocTax(tax.getId(), tax.getName(), tax.getRate().toString(),
                  documentTax.getTaxableAmount().toString(), documentTax.getTaxAmount().toString());
              final BigDecimal taxAcctAmt = documentTax.getTaxAmount()
                  .multiply(percentage)
                  .divide(new BigDecimal("100"), 10, RoundingMode.HALF_UP);

              final String entryAmount = OBTVB_Utility.round(taxAcctAmt, docFAT.C_Currency_ID)
                  .toString();
              log4j.debug("Entry amount for tax (" + tax.getIdentifier() + "): " + entryAmount
                  + " (rounded from " + taxAcctAmt + ")");

              // Enable this for two different entries
              // Fact_Acct_Group_ID = SequenceIdData.getUUID();
              if (order.isSalesTransaction()) {
                fact.createLine(null, docTax.getAccount(DocTax.ACCTTYPE_TaxDue, as, conn),
                    docFAT.C_Currency_ID, "", entryAmount, Fact_Acct_Group_ID,
                    docFAT.nextSeqNo(getSeqNo()), docFAT.DocumentType, conn);
                fact.createLine(null, OBTVB_Utility.getTransitoryTaxAccount(tax.getId(), as, conn),
                    docFAT.C_Currency_ID, entryAmount, "", Fact_Acct_Group_ID,
                    docFAT.nextSeqNo(getSeqNo()), docFAT.DocumentType, conn);
              } else {
                fact.createLine(null, docTax.getAccount(DocTax.ACCTTYPE_TaxCredit, as, conn),
                    docFAT.C_Currency_ID, entryAmount, "", Fact_Acct_Group_ID,
                    docFAT.nextSeqNo(getSeqNo()), docFAT.DocumentType, conn);
                fact.createLine(null, OBTVB_Utility.getTransitoryTaxAccount(tax.getId(), as, conn),
                    docFAT.C_Currency_ID, "", entryAmount, Fact_Acct_Group_ID,
                    docFAT.nextSeqNo(getSeqNo()), docFAT.DocumentType, conn);
              }
            }
          } else {
            log4j.error("Neither an order nor an invoice were detected");
          }
        }
      }
    } else {
      fact.createLine(null,
          docFAT.getAccountPayment(conn, transaction.getFinPayment().getPaymentMethod(),
              transaction.getFinPayment().getAccount(), as,
              transaction.getFinPayment().isReceipt()),
          docFAT.C_Currency_ID,
          !transaction.getFinPayment().isReceipt()
              ? transaction.getFinPayment().getAmount().toString()
              : "",
          transaction.getFinPayment().isReceipt()
              ? transaction.getFinPayment().getAmount().toString()
              : "",
          Fact_Acct_Group_ID, docFAT.nextSeqNo(SeqNo), docFAT.DocumentType, conn);
    }
    fact.createLine(null,
        docFAT.getAccountUponDepositWithdrawal(conn, transaction.getFinPayment().getPaymentMethod(),
            transaction.getAccount(), as, transaction.getFinPayment().isReceipt()),
        docFAT.C_Currency_ID, transaction.getDepositAmount().toString(),
        transaction.getPaymentAmount().toString(), Fact_Acct_Group_ID, docFAT.nextSeqNo(SeqNo),
        docFAT.DocumentType, conn);

    SeqNo = "0";
    return fact;
  }

}

/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2011 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */

package org.openbravo.obtvb.utillity;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

import javax.servlet.ServletException;

import org.apache.log4j.Logger;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.dal.service.OBQuery;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.erpCommon.ad_forms.Account;
import org.openbravo.erpCommon.ad_forms.AcctSchema;
import org.openbravo.model.common.currency.Currency;
import org.openbravo.model.financialmgmt.tax.TaxRateAccounts;

public class OBTVB_Utility {

  static Logger log4j = Logger.getLogger(OBTVB_Utility.class);

  public static Account getTransitoryTaxAccount(String strcTaxId, AcctSchema as,
      ConnectionProvider conn) {
    Account transitoryAccount = null;
    try {
      OBContext.setAdminMode(true);
      final StringBuilder whereClause = new StringBuilder();

      if (!strcTaxId.equals("")) {
        whereClause.append(" as tra ");
        whereClause.append(" where tra.tax.id = '" + strcTaxId + "'");
        whereClause.append(" and tra.accountingSchema.id = '" + as.m_C_AcctSchema_ID + "'");
      } else
        return null;

      final OBQuery<TaxRateAccounts> obqParameters = OBDal.getInstance().createQuery(
          TaxRateAccounts.class, whereClause.toString());
      // For Background process execution
      obqParameters.setFilterOnReadableClients(false);
      obqParameters.setFilterOnReadableOrganization(false);
      final List<TaxRateAccounts> taxRateAccounts = obqParameters.list();
      transitoryAccount = new Account(conn, taxRateAccounts.get(0).getObtvbTTransitoryAcct()
          .getId());
    } catch (ServletException e) {
      log4j.warn(e);
    } catch (IndexOutOfBoundsException e) {
      log4j.warn("No transitory account found for taxId: '" + strcTaxId + "' - Acct. Schema: '"
          + as.m_Name + "'");
    } catch (NullPointerException e) {
      log4j.warn("No transitory account found for taxId: '" + strcTaxId + "' - Acct. Schema: '"
          + as.m_Name + "'");
    } finally {
      OBContext.restorePreviousMode();
    }
    return transitoryAccount;
  } // getTransitoryTaxAccount

  public static BigDecimal round(final BigDecimal amount, final String currencyId) {
    try {
      OBContext.setAdminMode(true);
      final Currency currency = OBDal.getInstance().get(Currency.class, currencyId);
      return amount.setScale(currency.getStandardPrecision().intValue(), RoundingMode.HALF_UP);
    } finally {
      OBContext.restorePreviousMode();
    }
  }

}
